#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import time
import logging
import os

from statemachine import state_machine, state, on_enter, transition, on_exit, task, global_task, master_state
import logger


_logger = logging.getLogger(__name__)


class Timer:
    def __init__(self, timeout):
        self.timeout = timeout
        self.start_time = None

    def check(self):
        if self.start_time is None:
            return False
        elif time.time() - self.start_time > self.timeout:
            return True
        else:
            return False

    def start(self):
        self.start_time = time.time()

@state
class TestState:
    def __init__(self):
        self.timer = Timer(5)

    @on_enter
    def enter(self):
        self.timer.start()

    @transition
    def switch(self):
        if self.timer.check():
            return Move(x=200, y=200)

@state
class ExitState:
    @on_enter
    def enter(self):
        _logger.info("x" * 40)
        _logger.info("done")
        _logger.info("x" * 40)

@state
class Move:
    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.timer = Timer(0.5)

    @on_enter
    def enter(self):
        _logger.info("Move from ({}/{}) to ({}/{})".format("nix", "nix", self.x, self.y))
        self.x *= 100
        self.timer.start()

@master_state
class MasterState:
    def __init__(self):
        self.task_timer = Timer(0.5)
        self.exit_timer = Timer(2)

    @on_enter
    def enter(self):
        self.task_timer.start()
        self.exit_timer.start()

    @task
    def task(self):
        if self.task_timer.check():
            _logger.debug("Debug message")
            _logger.info("Info message")
            _logger.warning("Warning message")
            _logger.error("Error message")
            _logger.critical(("Critical message"))

            self.task_timer.start()
            return True
        else:
            return True

    @transition
    def switch_to_TestState(self):
        if self.exit_timer.check():
            return TestState()


if __name__ == "__main__":
    logger.init_logger()
    # logger.init_file_logger(os.path.abspath(__file__) + ".log")
    logger.set_modul_level("statemachine.state", logging.INFO)
    logger.set_modul_level("driver", logging.INFO)

    timer = Timer(15)
    timer.start()

    @global_task
    def game_over():
        if timer.check():
            _logger.debug("=" * 40)
            _logger.debug("=" * 40)
            _logger.debug("Stopping!")
            _logger.debug("=" * 40)
            _logger.debug("=" * 40)
            # power = Power()
            # hpower.switch_off("All")
            state_machine.stop()
        else:
            return True

    master = MasterState()
    master.start()


