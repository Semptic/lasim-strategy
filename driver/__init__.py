# -*- coding:utf-8 -*-

import logging

module_name = __name__[:len(".__init__.py")]

_logger = logging.getLogger(module_name)
_logger.setLevel(logging.INFO)

from .fahrwerk import Fahrwerk
from .power import Power
from .autorouter import Autorouter
from .sensorik import Sensorik
from .robocom import Robocom
from .rps import RPS
from .handhabung_klein import Handhabung_Klein
from .handhabung_groß import Handhabung_Groß
from .bildverarbeitung import Bildverarbeitung