# -*- coding:utf-8 -*-

from .basedriver import BaseDriver
from .fahrwerk import Fahrwerk
import logging

_logger = logging.getLogger(__name__)


class Autorouter(BaseDriver):

    MODULE_NAME = "ar"
    MODULE_PATH = "ar"

    def __init__(self):
        BaseDriver.__init__(self)
        self.fahrwerk = Fahrwerk()
        self.info = None

    def initialize(self):
        # Aufrufen nachdem Fahrwerk Initialiesiert ist?
        _logger.info("Initialisiere Autorouter")

    def _calc_route(self, x, y):
        fahrwerk_x = self.fahrwerk.x
        fahrwerk_y = self.fahrwerk.y

        _logger.info("Berechne Weg von {}/{} nach {}/{}".format(fahrwerk_x, fahrwerk_y, x, y))

        self.dbus_object.ARSetOwnPosition(fahrwerk_x, fahrwerk_y, self.fahrwerk.orientation)

        queue_id = self.fahrwerk.create_queue()
        self.info = self.dbus_object.ARCalcWayToB(x, y, 0, queue_id)
       # if self.info < 0:
        #    self.fahrwerk.quick_stop()

        _logger.debug("Rueckgabewert vom Autorouterberechnung: {}".format(self.info))
        return queue_id

    def move(self, x, y):
        _logger.info("Fahre Automatisch")
        #Gibt zurueck ob Autorouter rechnen kann oder nicht
        queue_id = self._calc_route(x, y)
        self.fahrwerk._run_queue(queue_id)
        self.fahrwerk.delete_all_queues()

        return self.info

    def makedynamicobjectrectangle(self, dynamic_object_id, x ,y ,dx ,dy):
        self.dbus_object.ARAddDynamicObstacleRect(dynamic_object_id, x ,y ,dx ,dy)

    def makedynamicobjectcircle(self, dynamic_object_id, x ,y ,r):
        self.dbus_object.ARSetAddDynamicObstacle(dynamic_object_id, x ,y,r)