# -*- coding:utf-8 -*-
"""
.. module:: basedriver
   :synopsis: Dieses Modul stellt die Basisklasse für
   alle Hardware Schnittstellen bereit.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging
import time

from .lasimdbus import get_dbus_object, add_dbus_signal_receiver
from statemachine.signal import Signal

# TODO Signale können möglicherweise über pygtk bzw. gobject verteilt werden.
# TODO Signale direkt über ihren namen und nicht über die allgemeinen funktionen verfügbar machen

_logger = logging.getLogger(__name__)

_signals = {}


class UnknownSignal(Exception):
    """
    Diese Exception wird geworfen, wenn
    das nicht bekannt ist.
    """
    pass


class BaseDriver:
    """
    Basisklasse für alle Driver-Module. Stellt funktionen bereit die
    alle Driver-Module beherrschen müssen.
    """
    SIGNAL_MAP = {}
    MODULE_NAME = None
    MODULE_PATH = None

    def __init__(self):
        """
        Initialisiert das zugehörige D-Bus-Objekt und registriert alle signal am
        D-Bus
        """
        global _signals

        # Objekt vom Dbus holen
        self.dbus_object = get_dbus_object(self.MODULE_NAME, self.MODULE_PATH)

        # Überprüfen ob die Klasse schon mal initialsiert wurde
        # und somit die signal handler schon angelegt wurden
        if self.__class__.__name__ not in _signals.keys():
            # Wenn nicht in der liste anlegen
            _signals[self.__class__.__name__] = {}

        self.signals = _signals[self.__class__.__name__]

        # Alle in der Klasse definierte Signale erstellen
        for signal in self.SIGNAL_MAP:
            self.create_signal(signal)

    # TODO Nicht mehr gebrauchte Signale löschen
    def create_signal(self, signal_name):
        """
        Meldet ein signal am D-Bus an, sollte es noch nicht angemeldet sein.

        :param signal_name: Name des Signals.
        :type  signal_name: str

        :raises UnknownSignal: wenn versucht wird ein nicht in der Klasse gespeichertes Signal zu erstellen.
        """
        try:
            # Umwandeln des "schönen namens" in den des D-Bus signals
            dbus_signal_name = self.SIGNAL_MAP[signal_name]
            setattr(self, signal_name, lambda: self.get_signal_condition(signal_name))
        except KeyError:
            raise UnknownSignal

        # Nur wenn das signal noch nicht erstellt wurde
        # (also nicht in der globalen liste aller
        # Signale vorhanden ist) den signal handler
        # anlegen und mit dem Signal verbinden
        if signal_name not in self.signals.keys():
            _logger.info("Creating new signal for {}".format(signal_name))

            self.signals[signal_name] = {"conditions": [], "args": None, "receiving_time": time.time()}

            # Erstellen der handler funktion
            def signal_handler(*args):
                _logger.debug("Handling signal {}".format(signal_name))

                # Alle gespeicherten Bedingungen setzen
                for condition in self.signals[signal_name]["conditions"]:
                    condition.set_flag()

                # Speichern der Signal argumente und der zeit
                self.signals[signal_name]["args"] = args
                self.signals[signal_name]["receiving_time"] = time.time()

            # Signal mit der handler funktion verknüpfen
            add_dbus_signal_receiver(self.MODULE_NAME, self.MODULE_PATH, dbus_signal_name, signal_handler)

    def get_signal_condition(self, signal_name):
        """
        Deprecated! Stattdessen einfach signal_name als Funktion aufrufen

        Erzeugt eine Bedingung für das übergebene Signal und gibt sie zurück.

        :param signal_name: Name des Signals.
        :type  signal_name: str

        :returns: Mit dem Signal verbundene Condition
        :rtype:   Condition

        :raises UnknownSignal: wenn versucht wird auf ein nicht erzeugtes Signal zuzugreifen.
        """
        # Bedingung erzeugen
        signal = Signal(lambda: (self.signals[signal_name]["receiving_time"], self.signals[signal_name]["args"]))
        signal.start()
        try:
            # Bedingung mit dem Signal verknüpfen
            self.signals[signal_name]["conditions"].append(signal)
        except KeyError:
            raise UnknownSignal

        return signal

    def _get_signal_parameter(self, signal_name):
        """
        Gibt die letzten vom angegebenen Signal empfangenen parameter zurück.

        Die Rückgabe enthält einen Zeitstempel der angibt, wann das
        Signal das letzte mal ausgelöst wurde.


        :returns: Zeitstempel und dem Signal zugeordneten Parameter. Aufbau: (Zeitstempel, (arg1, arg2, argN))
        :rtype:   tuple

        :raises UnknownSignal: wenn versucht wird auf ein nicht erzeugtes Signal zuzugreifen.
        """
        return self.signals[signal_name]["receiving_time"], self.signals[signal_name]["args"]

    def initialize(self):
        """
        Initialisiert das Modul. Diese funktion ist leer und
        muss vom Modul selbst überschrieben werden.
        """
        pass

