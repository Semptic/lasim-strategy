# -*- coding:utf-8 -*-

import logging
import time

from .basedriver import BaseDriver

_logger = logging.getLogger(__name__)


class Bildverarbeitung(BaseDriver):

    MODULE_NAME = "img"
    MODULE_PATH = "img"

    SIGNAL_MAP = {"fire_data": "fireData",
                  "fruit_data": "fruitData"}

    def initialize(self):
        _logger.info("Schalte die Bildverarbeitung an!")

    def find_fires(self):

        return self.dbus_object.findFires()

    def find_fruits(self):

        return self.dbus_object.findFruits()
