# -*- coding:utf-8 -*-
"""
.. module:: fahrwerk
   :synopsis: Dieses Modul stellt eine einfache Schnittstelle zwischen
    der Hardware des Fahrwerks und der Strategie zur Verfügung.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging
import time

from .basedriver import BaseDriver
from .power import Power

# TODO Fehlerbehebung
# TODO Fehlende Methoden und Signale implementieren und Dokumentieren
# TODO Zu beachten im Kommentar überprüfen und überarbeiten

_logger = logging.getLogger(__name__)


class Fahrwerk(BaseDriver):
    """
    Diese Klasse ermöglicht das Fahren mit dem Roboter.

    Signale:
        position_reached
            D-Bus-Name:
                PosReached

            Parameter:
                Keine

        quick_stopped
            D-Bus-Name:

            Parameter:
                Keine

    Beispiel:
        Roboter auf Position (500/500) fahren lassen:
        >>> from driver import Fahrwerk

        >>> fahrwerk = Fahrwerk()
        >>> fahrwerk.initialize()
        >>> fahrwerk.move(500, 500)

    Zu beachten:
        Bei einem Aufruf von blockierenden Fahrfunktionen, kehrt die Funktion sofort nach Auftreten
        einer Kollision zurück. Allerdings behält das Fahrwerk den letzten Fahrbefehl und versucht
        ihn weiter auszuführen. Es wird empfohlen den Fahrbefehl abzubrechen oder mit einem neuen Befehl
        zu überschreiben. In Zukunft wird evtl. das Verhalten dahingegen modifiziert,
        dass bei einer Kollision der Fahrbefehl samt der Funktion Abgebrochen wird.
        Zur Erzeugung von einigen Signalen muss erst die CAN-Schnittstelle des
        Fahrwerks über FWCANConfig Konfiguriert werden.
    """
    MODULE_NAME = "fw"
    MODULE_PATH = "fw"

    SIGNAL_MAP = {"position_reached": "PosReached",
                  "quick_stopped": "QuickStopDone"}

    @property
    def x(self):
        """
        Die aktuelle x-Koordinate der Odometrie.

        :returns:   Die aktuelle x-Koordinate
        :rtype:     float
        """
        return self.dbus_object.FWX()

    @property
    def y(self):
        """
        Die aktuelle y-Koordinate der Odometrie.

        :returns:   Die aktuelle y-Koordinate
        :rtype:     float
        """
        return self.dbus_object.FWY()

    @property
    def orientation(self):
        """
        Die aktuelle Ausrichtung der Odometrie.

        :returns:   Die aktuelle Ausrichtung
        :rtype:     float
        """
        return self.dbus_object.FWA()

    def initialize(self):
        """
        Initialisiert das Fahrwerk mit Standardeinstellungen
        und schaltet die Stromversorgung ein.

        Diese Funktion darf nur einmal beim Start aufgerufen
        werden.

        Standardeinstellungen sind:
            Geschwindigkeit:
                fahren: 300
                drehen: 300
            Startposition:
                x-Koordinate: 150 mm
                y-Koordinate: 150 mm
                Ausrichtung:  45 grad
        """
        _logger.info("Initialisiere Fahrwerk")
        Power().switch_on(self)
        # Nach dem Anschalten warten
        # Zeit ist grob getestet,
        # sollte aber nochmals genauer
        # getestet werden um
        # wartezeiten zu verkürzen
        time.sleep(5)
        self.reset()
        #roboter + 10cm puffer
        self.set_position(3000-95, 550, 180)
        self.set_speed(300, 300)

    def set_speed(self, straight, turn):
        """
        Setzen der Geschwindigkeit.

        :param straight: Fahrgeschwindigkeit
        :type  straight: int

        :param turn: Drehgeschwindigkeit
        :type  turn: int
        """
        _logger.debug("Setze Geschwindigkeit auf {}, {}".format(straight, turn))
        self.dbus_object.FWSetSpeeds(straight, turn)

    def set_position(self, x, y, orientation):
        """
        Setzt die Position des Fahrwerks.

        :param x: X-Koordinate
        :type  x: float

        :param y: Y-Koordinate
        :type  y: float

        :param orientation: Ausrichtung
        :type  orientation: float
        """
        _logger.debug("Setze Position auf ({}/{}), {}".format(x, y, orientation))
        self.dbus_object.FWSetPos(x, y, orientation)

    def move(self, x, y, backwards=None):
        """
        Gibt dem Roboter den Befehl
        eine bestimmte Koordinate anzufahren.

        :param x: X-Koordinate
        :type  x: float

        :param y: Y-Koordinate
        :type  y: float

        :param backwards: True, wenn der Roboter rückwärts an das Ziel fahren soll. None, wenn
                          das Fahrwerk entscheiden soll.
        :type  backwards: bool
        """
        _logger.info("Fahre von {}/{} nach {}/{}".format(self.x, self.y, x, y))

        self.dbus_object.MoveAbsNB(x, y, 2 if backwards is None else 0 if backwards else 1)

    def move_relativ(self, x, y, backwards=None):
        """
        Gibt dem Roboter den Befehl
        eine bestimmte Koordinate anzufahren.

        :param x: X-Koordinate
        :type  x: float

        :param y: Y-Koordinate
        :type  y: float

        :param backwards: True, wenn der Roboter rückwärts an das Ziel fahren soll. None, wenn
                          das Fahrwerk entscheiden soll.
        :type  backwards: bool
        """
        _logger.info("Fahre von {}/{} nach {}/{}".format(self.x, self.y, x, y))

        self.dbus_object.MoveRelNB(x, y, 2 if backwards is None else 0 if backwards else 1)


    def turn(self, orientation, clockwise=None):
        """
        Gibt dem Roboter den Befehl sich auf eine
        bestimmte Ausrichtung zu drehen,

        :param orientation: Ausrichtung
        :type  orientation: float

        :param clockwise: Bei True dreht sich der Roboter im Uhrzeigersinn. Bei
                          None entscheidet das Fahrwerk.
        :type  clockwise: bool
        """
        _logger.debug("Drehe von {} nach {}".format(self.orientation, orientation))
        self.dbus_object.TurnAbsNB(orientation, 2 if clockwise is None else 4 if clockwise else 3)

    def reset(self):
        """
        Setzt das Fahrwerk zurück.
        """
        self.dbus_object.FWReset()
        #resolved Issue #7 :D
        #bei Reset bootet die Fahrwerkplatine neu
        #deshalb sollte der der Roboter kurz warten.
        time.sleep(2)

    def create_queue(self):
        """
        Erstelle id für eine Queue.

        keine Parameter.
        """
        _logger.debug("Erstelle ID für FW-Queue")
        return self.dbus_object.FWQueueStart()

    def delete_all_queues(self):
        """
        Loesche alle queues.

        keine Parameter.
        """
        _logger.debug("Lösche alle Queues")
        self.dbus_object.FWQueueDeleteAll()

    def _append_vector(self, queue_id, x, y):
        """
        Fügt Vector zur Queue hinzu.

        :param queue_id: Id der Queue zu der der Vektor hinzugefügt wird
        :type  queue_id: int

        :param x: X-Koordinate
        :type  x: float

        :param y: Y-Koordinate
        :type  y: float
        """
        _logger.debug("Füge Vector {}/{} zur Queue (id: {}) hinzu".format(x, y, queue_id))

        self.dbus_object.FWQueueAppendVector(queue_id, x, y)

    def _run_queue(self, queue_id):
        """
        Führt alle Vectoren (Fahrbefehle) in der Queue mit der übergebenen id aus.

        :param x: id
        :type  x: int
        """
        _logger.debug("Fahrbefehle aus Queue {}".format(queue_id))
        self.dbus_object.FWDoFullQueueNB(queue_id)

    def _do_queue_link(self, queue_id):

        self.dbus_object.FWDoFullQueueLinkNB(queue_id)

    def clear_queue(self,queue_id):
        """
        leert vektorliste der Queue (id)

        :param x: id
        :type  x: int
        """
        _logger.debug("Leere Vektorliste von Queue {}".format(queue_id))
        self.dbus_object.FWQueueClear(queue_id)

    def get_queue_size(self, queue_id):
        """
        gibt größe der Queue (id) zurück.

        :param x: id
        :type  x: int
        """
        _logger.debug("Groesse der Queue {}:".format(queue_id))
        self.dbus_object.FWQueueGetSize(queue_id)

    def quick_stop(self):
        self.dbus_object.FWQuickstop()


