__author__ = 'markus'


import logging, time

from .basedriver import BaseDriver

_logger = logging.getLogger(__name__)


class Handhabung_Groß(BaseDriver):
    """
       Hier gibts später noch info
    """
    MODULE_NAME = "handhabung"
    MODULE_PATH = "handhabung"

    SIGNAL_MAP = {"handhabung_done": "HHdone",
                  "handhabung_failed": "HHfailed"}

    #dokumentation kommt später da wir noch nicht soviele infos haben. Implementieren nur schonmal die dbusfunktionen

    def initialize(self):

        _logger.info("Initialisiere Handhabung")

        return self._init()

    def _init(self):

        return self.dbus_object.HHinit()

    def drop_fruits(self):

        return self.dbus_object.HHdropFruits()

    def grab_fruits(self):

        return self.dbus_object.HHgrapFruits()

    def move_to_axis_pos(self, pos):
        # 0 - 680
        return self.dbus_object.HHtoAxisPos(pos)

    def flip_fire(self):
        #muss das feuer schon haben
        #fährt hoch 720 oder 730
        #fährt seinen Penis aus
        #läst feuer los
        #fährt penis ein
        #fährt wanne nicht runter
        #kralle ist offen
        _logger.info("Flip Fire Mehtode")
        return self.dbus_object.HHflipFire()

    def extendt_grabber(self):

        self.dbus_object.HHmoveRightGrabber(0)
        return self.dbus_object.HHextendtGrapper()

    def move_left_grabber(self, pos):
        # 0 == zurück // 1 == Nach vorn
        return self.dbus_object.HHmoveLeftGrabber(pos)

    def move_right_grabber(self, pos):
        # 0 == zurück // 1 == Nach vorn
        return self.dbus_object.HHmoveRightGrabber(pos)

    def move_comb(self, pos):
        # 0 == unten // 1 == 45° // 2 == 90°
        return self.dbus_object.HHmoveComb(pos)

    def moveStake(self, pos):
        # 0 == eingefahren // 1 == ausgefahren
        return self.dbus_object.HHmoveStake(pos)

    def move_container(self, pos):
        # 0 == standard // 1 == ausgeklappt
        return self.dbus_object.HHmoveContainer(pos)

    def close_claw(self):

        return self.dbus_object.HHmoveTongsForward()

    def open_claw(self):

        return self.dbus_object.HHmoveTongsBackward()

    def stop_claw(self):

        return self.dbus_object.HHstopTongs()

    def status(self):

        return self.dbus_object.HHstatus()

    def is_busy(self):

        return self.dbus_object.HHisBusy()

    def grab_right_fruit(self):

        #setzt nur einen flag
        #name könnte falsch sein
        self.dbus_object.HHretractRight()

        return self.grab_fruits()

    def grab_left_fruit(self):

        #setzt nur einen flag
        #name könnte falsch sein
        self.dbus_object.HHretractLeft()

        return self.grab_fruits()

    def grab_fire_2(self):
        #wann muss weitgenug oben sein
        self.close_claw()
        _logger.info("Kralle schließen")
        #höhe anpassen zwechs ablegen?
        _logger.info("Kralle auf 0")
        return self.move_to_axis_pos(0)

    def grab_fire_1(self):
        self.open_claw()
        _logger.info("Kralle öffnen")
        _logger.info("Kralle auf 0")
        return self.move_to_axis_pos(0)

    def pull_fire(self):
        self.close_claw()
        return self.move_to_axis_pos(350)





