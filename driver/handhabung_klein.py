import logging

from .basedriver import BaseDriver

_logger = logging.getLogger(__name__)


class Handhabung_Klein(BaseDriver):
    """
       Hier gibts später noch info
    """
    MODULE_NAME = "handhabung"
    MODULE_PATH = "handhabung"

    SIGNAL_MAP = {"handhabung_done": "HHdone",
                  "handhabung_status": "HHstatus",
                  "handhabung_busy": "HHisBusy"}

    #dokumentation kommt später da wir noch nicht soviele infos haben. Implementieren nur schonmal die dbusfunktionen

    def fire_top_row(self):
        return self.dbus_object.HHFireBall1_3()

    def fire_bottom_row(self):
        return self.dbus_object.HHFireBall4_6()

    def fire_net(self):
        return self.dbus_object.HHFireNet()

    def top_row_down(self):
        return self.dbus_object.HHUpBulletPos2()

    def bottom_row_down(self):
        return self.dbus_object.HHDownBulletPos2()

    def top_row_up(self):
        return self.dbus_object.HHUpBulletPos1()

    def bottom_row_up(self):
        return self.dbus_object.HHDownBulletPos1()