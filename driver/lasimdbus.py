# -*- coding:utf-8 -*-
"""
.. module:: dbus
   :synopsis: Dieses Modul stellt eine einfache Schnittstelle
    für die D-Bus Kommunikation zur Verfügung. Sollte nicht außerhalb
    sollte des driver Moduls verwendet werden.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import os
import logging

import dbus
from dbus.mainloop.glib import DBusGMainLoop


_logger = logging.getLogger(__name__)

BUS_PREFIX = "org.kalu"
BUS_OS_ADDRESS_NAME = "DBUS_MAGIC_BUS_ADDRESS"

__magic_bus = None
__dbus_objects = {}

DBusGMainLoop(set_as_default=True)


class NoDBus(Exception):
    """
    Diese Exception wird geworfen, wenn kein D-Bus gefunden werden konnte.
    """
    pass


class ModuleNotFound(Exception):
    """
    Diese Exception wird geworfen, wenn ein Modul
    nicht auf dem D-Bus gefunden wurde.
    """
    pass


def get_dbus():
    """
    Gibt den D-Bus zurück.

    :returns:   Der D-Bus
    :rtype:     dbus

    :raises NoDBus: wenn kein D-Bus gefunden werden kann.
    """
    global __magic_bus

    # D-Bus nur holen, fall dieser noch nicht geholt
    # wurde
    if __magic_bus is None:
        magic_bus_address = os.getenv(BUS_OS_ADDRESS_NAME)
        try:
            # Ist die D-Bus adresse im os festgelegt?
            if magic_bus_address is not None:
                try:
                    # Wenn ja versuche den den D-Bus zu holen
                    __magic_bus = dbus.bus.BusConnection(
                        magic_bus_address)
                except dbus.exceptions.DBusException:
                    # Falls dies nicht funktioniert hat
                    # holen den normalen dbus
                    _logger.warning("Magic-D-Bus nicht verfügbar")
                    __magic_bus = dbus.SessionBus()
            else:
                # Wenn nein, hole den normalen dbus
                _logger.warning("DBUS_MAGIC_BUS_ADDRESS nicht gesetzt")
                __magic_bus = dbus.SessionBus()
        except dbus.exceptions.DBusException:
            raise NoDBus("Kein Dbus verfügbar")

    # D-Bus namen festlegen
    __magic_bus.request_name("{}.Strategie".format(BUS_PREFIX))

    return __magic_bus


def get_dbus_object(module_name, module_path):
    """
    Durchsucht den D-Bus nach
    dem angegebenen Modul und gibt es zurück.

    :returns: D-Bus Modul
    :rtype:   object

    :raises ModuleNotFound: wenn versucht wird auf ein nicht am D-Bus verfügbares Modul zuzugreifen.
    """
    try:
        # Versuche das gespeicherte D-Bus objekt zu laden
        dbus_object = __dbus_objects[module_name, module_path]
    except KeyError:
        # Falls dies nicht möglich ist, hole das objekt vom D-Bus
        _logger.debug("{} wird vom D-Bus geladen".format(module_name))
        try:
            interface = get_dbus().get_object("{0}.{1}".format(BUS_PREFIX, module_name), "/{0}".format(module_path))
            dbus_object = dbus.Interface(interface, "{0}.{1}".format(BUS_PREFIX, module_name))
            __dbus_objects[module_name, module_path] = dbus_object
        except dbus.exceptions.DBusException:
            raise ModuleNotFound("{} konnte am D-Bus nicht gefunden werden.".format(module_name))

    return dbus_object


def add_dbus_signal_receiver(module_name, module_path, signal_name, callback):
    """
    Registriert eine Call-Back-Funktion die aufgerufen wird, wenn
    das entsprechende Signal über den D-Bus gesendet wird.

    :param module_name: D-Bus-Name des Moduls
    :type  module_name: str

    :param module_path: Pfad zum D-Bus-Modul
    :type  module_path: str

    :param signal_name: Name des Signals
    :type  signal_name: str

    :param callback:    Call-Back-Funktion
    :type  callback:    function
    """
    interface = "{0}.{1}".format(BUS_PREFIX, module_name)

    _logger.debug("Signal {} - {} wird an D-Bus angemeldet".format(interface, signal_name))

    get_dbus().add_signal_receiver(callback,
                                   dbus_interface=interface,
                                   signal_name=signal_name,
                                   path="/{}".format(module_path))