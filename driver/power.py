# -*- coding:utf-8 -*-
"""
.. module:: power
   :synopsis: Dieses Modul stellt eine einfache Schnittstelle zwischen
    der Hardware der Energieversorgung und der Strategie zur Verfügung.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging
import inspect
import time

from .basedriver import BaseDriver

# TODO Fehlende Methoden und Signale implementieren und Dokumentieren

_logger = logging.getLogger(__name__)


class Power(BaseDriver):
    """
    Klasse zum Steuern der Energieversorgung

    Signale:
        emergency_switch
            D-Bus-Name:
                PWEStopStateChanged

            Parameter:
                State (int): 1: Notaus draußen 0: Notaus gedrückt

    Beispiel:
        Stromversorgung für das Fahrwerk einschalten
        >>> from driver import Power
        >>> from driver import Fahrwerk

        >>> power = Power()
        >>> power.initialize()  # Nur einmal in der gesamten Strategie aufrufen
        >>> power.switch_on("Fahrwerk")  # als string
        oder
        >>> power.switch_on(Fahrwerk)  # als Klasse

        oder
        >>> fahrwerk = Fahrwerk()
        >>> power.switch_on(fahrwerk)  # als Instanz

        Abschalten der Stromversorgung der Handhabung und des Fahrwerks
        >>> from driver import Power

        >>> power = Power()
        >>> power.initialize()  # Nur einmal in der gesamten Strategie aufrufen
        >>> power.switch_off("Fahrwerk", "Handhabung")
        auch hier gelten die beiden anderen Varianten,
        bei denen man die Klasse oder eine Instanz
        übergeben kann.
    """
    MODULE_NAME = "pw"
    MODULE_PATH = "pw"

    SIGNAL_MAP = {"emergency_switch": "PWEStopStateChanged"}

    # TODO Liste verfollständigen
    # TODO Sowas in config files auslagern?

    MODULE_MAP = {"Controller": 1,
                  "Handhabung": 16,
                  "Fahrwerk": 10,
                  "All": 254}

    def initialize(self):
        """
        Initialisiert die Energieversorgung und deaktiviert die
        Stromversorgung für alle Module außer die der Controller,
        welche eingschaltet wird.

        Diese Funktion darf nur einmal beim Start aufgerufen
        werden.
        """
        _logger.info("Initialisiere Energieversorgung")
        #self.switch_off("All")
        #time.sleep(5)
        #self.switch_on("All")
        #self.switch_on("Controller")
        # Warten bis auch der langsamste
        # Controller kapiert hat
        # was sache ist
        #time.sleep(5)

    def __switch(self, module, switch_on):
        """
        Aktiviert oder deaktiviert die Energieversorgung
        für ein oder mehrere Module.

        :param module: Die Module die (de)aktiviert werden.
        :type  module: BaseDriver Klasse oder BaseDriver Instanz oder str oder liste aus vorhergehenden

        :param switch_on: True wenn die Module angeschaltet werden, False beim abschalten
        :type  switch_on: bool
        """
        # D-Bus funktionen, je nach dem ob an
        # oder abgeschaltet werden soll festlegen
        if switch_on:
            switch_function = self.dbus_object.PWSwitchOn
        else:
            switch_function = self.dbus_object.PWSwitchOff

        # Der aufruf über den D-Bus erfolgt nur mit strings,
        # alle anderen typen werden umgewandelt und
        # diese funktion wird rekursiv mit den umgewandelten
        # typen aufgerufen
        if isinstance(module, list) or isinstance(module, tuple):
            for module in module:
                self.__switch(module, switch_on)
        elif isinstance(module, BaseDriver):
            # Wenn klasse
            self.__switch(module.__class__.__name__, switch_on)
        elif inspect.isclass(module) and BaseDriver in module.__bases__:
            # Wenn instanz und Kinderklasse von BaseDriver
            self.__switch(module.__name__, switch_on)
        else:
            try:
                switch_function(self.MODULE_MAP[module])
                _logger.info("Switch power {} for {}".format("on" if switch_on else "off", module))
            except KeyError:
                _logger.warning("Unknown module {}".format(module))

    def switch_on(self, module):
        """
        Aktiviert die Energieversorgung ein oder mehrere Module.

        :param module: Die Module die aktiviert werden.
        :type  module: BaseDriver Klasse oder BaseDriver Instanz oder str oder liste aus vorhergehenden
        """
        self.__switch(module, True)

    def switch_off(self, module):
        """
        Deaktiviert die Energieversorgung ein oder mehrere Module.

        :param module:  Die Module die deaktiviert werden.
        :type  module: BaseDriver Klasse oder BaseDriver Instanz oder str oder liste aus vorhergehenden
        """
        self.__switch(module, False)