# -*- coding:utf-8 -*-
"""
.. module:: robocom
   :synopsis: Dieses Modul stellt eine einfache Schnittstelle zwischen
    den Robotern und der Strategie zur Verfügung.

.. moduleauthor:: Tobias Madl <madl@hm.edu>
"""
import logging
import time

from .basedriver import BaseDriver

_logger = logging.getLogger(__name__)

class Robocom(BaseDriver):

    MODULE_NAME = "rc"
    MODULE_PATH = "rc"

    SIGNAL_MAP = {"roboter_position": "RCPos",
                  "gegner_position": "RCEnemyPos",
                  "game_start_robocom": "RCStart",
                  "recieved_int": "RCGotInt",
                  "recieved_float": "RCGotFloat"}

    def initialize(self):
        _logger.info("Initialisiere Robocom")
        self.dbus_object.RCSendContinuesly(1)
        time.sleep(2)

    def send_int(self, cmd, data):
        _logger.debug("Schicke Integer Befehl: {}, Daten: {}".format(cmd, data))
        self.dbus_object.RCSendInt(cmd,data)

    def send_float(self, cmd, data):
        _logger.debug("Schicke Float Befehl: {}, Daten: {}".format(cmd, data))
        self.dbus_object.RCSendFloat(cmd,data)