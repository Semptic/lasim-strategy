import logging
import time

from .basedriver import BaseDriver
from .power import Power

_logger = logging.getLogger(__name__)


class RPS(BaseDriver):
    """
       Hier gibts später noch info
    """
    MODULE_NAME = "rps"
    MODULE_PATH = "rps"

    SIGNAL_MAP = {
        "rps_position": "RPSpos",
        "rps_state": "RPSState"   #Wird das Signal überhaupt benötigt?
    }

    def initialize(self, remote_position_system_id):
        _logger.info("Schalte die RPS an!")
        self.dbus_object.RPSSetBTState(remote_position_system_id, 1)
        time.sleep(2)
        self.dbus_object.RPSSendPositionContinuesly(remote_position_system_id, 1)
        time.sleep(2)

    def get_position(self, remote_position_system_id):
        #ich vermute deas Req für request steht es könnte auch für required stehen glaub ich aber net
        return self.dbus_object.RPSReqPos(remote_position_system_id)

    def RPSSetStartPos(self, remote_position_system_id, team):
        #Noch nicht sicher, ob diese Methode benötigt wird
        return self.dbus_object.RPSSetStartPos(remote_position_system_id, team)

    def get_status(self, remote_position_system_id):
        return self.dbus_object.RPSReqStatus(remote_position_system_id)