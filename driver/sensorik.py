# -*- coding:utf-8 -*-

import logging
import time

from .basedriver import BaseDriver

_logger = logging.getLogger(__name__)


class Sensorik(BaseDriver):

    MODULE_NAME = "sensorik"
    MODULE_PATH = "sensorik"

    SIGNAL_MAP = {"game_start": "SENSStart",
                  "collision": "SENSKoll"}

    def initialize(self, us_mode, start_stop):
        _logger.info("Schalte die Sensorik an!")
        #(0 = kollisionschutz,
        # 0=vorwaerts,
        # 0 = USS enable,
        # 1= Halsensor aus,
        # 1= Farbsensor aus,
        # Distance der Sensoren nicht benutzt(von 1-255)
        #kommentar wird noch verschoenert
        self.dbus_object.SENSconfig(us_mode, start_stop, start_stop)

    def sens_get_distance(self, sensor_id):
        return self.dbus_object.SENSgetdist(sensor_id)




#das sind alle funktionen die wir brauchen werden die anderen sind nur zum debuggen da ob sensorik geht oder net