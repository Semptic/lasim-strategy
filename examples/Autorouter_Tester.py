# !/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import os
import sys

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

from statemachine import state, on_enter, transition, task, state_machine
from driver import Fahrwerk, Autorouter
from driver import Power

import logger


_logger = logging.getLogger(__name__)


@state
class Pentagram:
    def __init__(self):
        self.flag = 0
        Power().initialize()
        Fahrwerk().initialize()
        Autorouter().initialize()

        self.positions = iter(
            ((400, 500),
             (2100, 1000),
             (400, 1500),
             (400, 1500),
             (1600, 300),
             (1600, 1700),
             (242.5, 242.5))
        )

        self.done = False

    @transition
    def edge(self):
        try:
            x, y = self.positions
            return AutoMove(x, y)
        except StopIteration:
            self.done = True
            return TurnBack()

    @task
    def exit(self):
        if self.done:
            state_machine.stop()


@state
class AutoMove:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.autorouter = Autorouter()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))

        self.position_reached.reset()
        self.autorouter.move(self.x, self.y)

    @transition
    def done(self):
        if self.position_reached:
            return Pentagram()


@state
class TurnBack:
    @on_enter
    def enter(self):
        fahrwerk = Fahrwerk()
        fahrwerk.turn(45, True)


if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.INFO)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie zum fahren eines Rechtecks")

    master = Pentagram()
    master.start()