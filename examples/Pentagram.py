# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

import logging

import logger
from driver import Fahrwerk, Power, Autorouter
from statemachine import state_machine, master_state, on_enter, transition

_logger = logging.getLogger(__name__)


@master_state
class Intelligence:
    def __init__(self):
        self.power = Power()
        self.power.initialize()

        self.fahrwerk = Fahrwerk()
        self.fahrwerk.initialize()
        self.fahrwerk.set_speed(800, 1000)

        self.autorouter = Autorouter()
        self.autorouter.initialize()

        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def _enter(self):
        self.position_reached.reset()

        self.autorouter.move(400, 500)
        self.autorouter.move(2100, 1000)
        self.autorouter.move(400, 1500)
        self.autorouter.move(1600, 300)
        self.autorouter.move(1600, 1700)

    @transition
    def done(self):
        if self.position_reached.check():
            state_machine.stop()


if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.WARNING)
    logger.set_modul_level("driver", logging.INFO)

    _logger.info("Strategie zum fahren eines Pentagrams")

    master = Intelligence()
    master.start()


