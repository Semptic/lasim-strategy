# !/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import os
import sys

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

from statemachine import master, state, master_state, transition, on_enter, on_exit
from driver import Fahrwerk
from driver import Power

import logger


_logger = logging.getLogger(__name__)


@master_state
class Rectangle:
    def __init__(self):
        Power().initialize()
        Fahrwerk().initialize()

        self.flag = 0
        self.x_rec = 600
        self.y_rec = 600
        self.x_board = 3000
        self.y_board = 2000

    @transition
    def first_position(self):
        if self.flag == 0:
            return Move(self.x_rec, self.y_rec)

    @transition
    def second_position(self):
        if self.flag == 1:
            return Move(self.x_board - self.x_rec, self.y_rec)

    @transition
    def third_position(self):
        if self.flag == 2:
            return Move(self.x_board - self.x_rec, self.y_board - self.y_rec)

    @transition
    def fourth_position(self):
        if self.flag == 3:
            self.flag = -1
            return Move(self.x_rec, self.y_board - self.y_rec)

    @on_exit
    def exit(self):
        self.flag += 1


@state
class Move:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y)

    @transition
    def done(self):
        if self.position_reached.check():
            return Rectangle()


if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.DEBUG)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie zum fahren eines Rechtecks")

    master = Rectangle()
    master.start()