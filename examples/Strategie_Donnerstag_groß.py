# __author__ = 'markus'
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import time

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

import logging

import logger
from driver import Fahrwerk, Power, Autorouter
from statemachine import state, master_state, state_machine, on_enter, transition, on_exit, task, global_task
from test_2014 import StandingFire, BoarderFire, StandingFireHandler

_logger = logging.getLogger(__name__)


@master_state
class Intelligence:
    def __init__(self):
        _logger.info("Initialisiere Module")
        power = Power()
        power.initialize()


        self.emergency_switch = power.emergency_switch()

        fahrwerk = Fahrwerk()
        fahrwerk.initialize()
        fahrwerk.set_speed(2000, 2000)

        self.quick_stopped = fahrwerk.quick_stopped()


        Autorouter().initialize()


        self.team_colour = None

        self.route_1 = 0
        self.autorouterroute_1 = 1

        self.current_fire_on_field = None

        while self.team_colour != "red" and self.team_colour != "yellow":
            self.team_colour = input("Welche Teamfarbe? \n")

        self.standing_fires = []
        self.standing_fires.append(StandingFire(400, 1100, 0))
        self.standing_fires.append(StandingFire(900, 1600, 90))
        self.standing_fires.append(StandingFire(900, 600, -90))
        self.standing_fires.append(StandingFire(2100, 1600, 90))
        self.standing_fires.append(StandingFire(2100, 600, -90))
        self.standing_fires.append(StandingFire(2600, 1100, 180))


        self.fires_on_field_yellow = [self.standing_fires[0], self.standing_fires[1], self.standing_fires[3], self.standing_fires[5]]

        self.fires_on_field_red = [self.standing_fires[5], self.standing_fires[3], self.standing_fires[1], self.standing_fires[0] ]

        if self.team_colour == "yellow":
            self.fires_on_field_iterator = iter(self.fires_on_field_yellow)
            Autorouter().makedynamicobjectcircle(1, self.standing_fires[0].x, self.standing_fires[0].y, 140)
            Autorouter().makedynamicobjectcircle(2, self.standing_fires[1].x, self.standing_fires[1].y, 140)
            Autorouter().makedynamicobjectcircle(3, self.standing_fires[3].x, self.standing_fires[3].y, 140)
            Autorouter().makedynamicobjectcircle(4, self.standing_fires[5].x, self.standing_fires[5].y, 140)
        elif self.team_colour == "red":
            self.fires_on_field_iterator = iter(self.fires_on_field_red)
            Autorouter().makedynamicobjectcircle(1, self.standing_fires[5].x, self.standing_fires[5].y, 140)
            Autorouter().makedynamicobjectcircle(2, self.standing_fires[3].x, self.standing_fires[3].y, 140)
            Autorouter().makedynamicobjectcircle(3, self.standing_fires[1].x, self.standing_fires[1].y, 140)
            Autorouter().makedynamicobjectcircle(4, self.standing_fires[0].x, self.standing_fires[0].y, 140)

        self.boarder_fires_yellow = []
        self.boarder_fires_yellow.append(BoarderFire(0, 800, -90))
        self.boarder_fires_yellow.append(BoarderFire(1700, 2000, 180))

        self.boarder_fires_red = []
        self.boarder_fires_red.append(BoarderFire(1300, 2000, 0))
        self.boarder_fires_red.append(BoarderFire(3000, 800, -90))
        #self.fires_on_sides = [self.boarder_fires[0], self.boarder_fires[1], self.boarder_fires[2], self.boarder_fires[3]]


        _logger.info("Du hast Farbe {} gewählt".format(self.team_colour))
        if self.team_colour == "red":
            fahrwerk.set_position(3000 - 240, 185, 90)
        else:
            fahrwerk.set_position(240, 185, 90.0)

        state_machine.start_time = time.time()
        start_game_over(90)

    @on_enter
    def enter(self):
        _logger.info("Test Flag ist jetzt: {}".format(self.route_1))

    @transition
    def start(self):
        if self.emergency_switch() and self.emergency_switch()[1][0] == 1:
            self.route_1 = 0


    @task
    def handle_collision(self):
        if self.quick_stopped() and "FrescoMove" not in state_machine.last_state:
            _logger.warning("Collision occoured")
        else:
            return True

    @transition
    def one(self):
        if self.route_1 == 0:
            try:
                self.current_fire_on_field = next(self.fires_on_field_iterator)
                _logger.info(str(self.current_fire_on_field.x) + str(self.current_fire_on_field.y))
                handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
                self.route_1 = 1


                return Move(handler.start_point.x, handler.start_point.y)
            except StopIteration:
                self.route_1 = 6
                return Intelligence()

    @transition
    def two(self):
        if self.route_1 == 1:
            Autorouter().makedynamicobjectcircle(self.autorouterroute_1, -1000, -1000, 140)
            handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
            self.current_fire_on_field.flip(self.team_colour)
            self.route_1 = 0
            self.autorouterroute_1 += 1

            return KickFire(handler.flip_point.x, handler.flip_point.y)

    @task
    def done(self):
        if self.route_1 == 6:
            _logger.info("Fertig! Stoppe die Statemachine.")
            state_machine.stop()

    @on_exit
    def exit(self):
        _logger.info("Test Flag ist jetzt: {}".format(self.route_1))
        self.quick_stopped.reset()

        #   global testflag
        #  if testflag == 0:
        #      handler = Standing_Fire_Handler(sfire1, "yellow")
        #      self.force_next_state(Move(handler.start_point.x, handler.start_point.y, self))
        #  if testflag == 1:
        #      handler = Standing_Fire_Handler(sfire1, "yellow")
        #      self.force_next_state(Kick_Fire(handler.flip_point.x, handler.flip_point.y, self))
        #  if testflag == 2:
        #      handler = Standing_Fire_Handler(sfire2, "yellow")
        #      self.force_next_state(Move(handler.start_point.x, handler.start_point.y, self))
        #  if testflag == 3:
        #      handler = Standing_Fire_Handler(sfire2, "yellow")
        #      self.force_next_state(Kick_Fire(handler.flip_point.x, handler.flip_point.y, self))
        #
        #  testflag += 1

@state
class Move:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.autorouter = Autorouter()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.autorouter.move(self.x, self.y)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


@state
class KickFire:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, False)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()

@state
class FrescoMove:
    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, True)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


# Wird nicht mehr funktionieren da keine Globalen variablen mehr
# class MakeDynamicObject():
#     def set(self):
#         global fires_on_field
#         i = 0
#         autorouter = Autorouter()
#         for fire in fires_on_field:
#             i += 1
#             if fire.is_standing:
#                 autorouter.makedynamicobjectrectangle(i, fire.x, fire.y, 130, 130)
#             else:
#                 autorouter.makedynamicobjectrectangle(i, fire.x, fire.y, -500, -500)

def start_game_over(seconds):
    emergency_switch = Power().emergency_switch()
    emergency_switch.reset()
    @global_task
    def game_over():
        if (emergency_switch() and emergency_switch()[1][0] == 0) or time.time() - state_machine.start_time >= seconds:
            _logger.info("Spiel ist vorbei nach {}!".format(time.time() - state_machine.start_time))
            state_machine.stop()
            Fahrwerk().quick_stop()
            #poweroff(63)!
        else:
            return True

if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.INFO)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie zum fahren eines Pentagrams")

    master = Intelligence()
    master.start()
