#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import time

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

import logging

import logger
from driver import Fahrwerk, Power, Autorouter
from statemachine import state, master_state, state_machine, on_enter, transition, on_exit, task, global_task
from test_2014 import StandingFire, BoarderFire, StandingFireHandler, Configurations, Destination

_logger = logging.getLogger(__name__)

@master_state
class Intelligence:
    def __init__(self):
        _logger.info("Initialisiere Module")
        power = Power()
        power.initialize()
        configurations = Configurations()

        self.emergency_switch = power.emergency_switch()

        fahrwerk = Fahrwerk()
        fahrwerk.initialize()
        fahrwerk.set_speed(configurations.move_speed_max, configurations.turn_speed_max)

        self.quick_stopped = fahrwerk.quick_stopped()


        Autorouter().initialize()


        self.team_colour = None

        self.route_1 = 0
        self.autorouterroute_1 = 1

        self.current_fire_on_field = None

        while self.team_colour != "red" and self.team_colour != "yellow":
            self.team_colour = input("Welche Teamfarbe? \n")

        self.standing_fires = []
        self.standing_fires.append(StandingFire(configurations.standing_fire_0_position_x, configurations.standing_fire_0_position_y, configurations.standing_fire_0_angle))
        self.standing_fires.append(StandingFire(configurations.standing_fire_1_position_x, configurations.standing_fire_1_position_y, configurations.standing_fire_1_angle))
        self.standing_fires.append(StandingFire(configurations.standing_fire_2_position_x, configurations.standing_fire_2_position_y, configurations.standing_fire_2_angle))
        self.standing_fires.append(StandingFire(configurations.standing_fire_3_position_x, configurations.standing_fire_3_position_y, configurations.standing_fire_3_angle))
        self.standing_fires.append(StandingFire(configurations.standing_fire_4_position_x, configurations.standing_fire_4_position_y, configurations.standing_fire_4_angle))
        self.standing_fires.append(StandingFire(configurations.standing_fire_5_position_x, configurations.standing_fire_5_position_y, configurations.standing_fire_5_angle))


        self.fires_on_field_yellow = [self.standing_fires[0], self.standing_fires[1], self.standing_fires[3], self.standing_fires[5]]

        self.fires_on_field_red = [self.standing_fires[5], self.standing_fires[3], self.standing_fires[1], self.standing_fires[0]]


        if self.team_colour == "yellow":
            self.fires_on_field_iterator = iter(self.fires_on_field_yellow)
        elif self.team_colour == "red":
            self.fires_on_field_iterator = iter(self.fires_on_field_red)

        self.boarder_fires_yellow = []
        self.boarder_fires_yellow.append(BoarderFire(configurations.border_fire_0_position_x, configurations.border_fire_0_position_y, configurations.border_fire_0_angle))
        self.boarder_fires_yellow.append(BoarderFire(configurations.border_fire_2_position_X, configurations.border_fire_2_position_y, configurations.border_fire_2_angle))

        self.boarder_fires_red = []
        self.boarder_fires_red.append(BoarderFire(configurations.border_fire_1_position_x, configurations.border_fire_1_position_y, configurations.border_fire_1_angle))
        self.boarder_fires_red.append(BoarderFire(configurations.border_fire_3_position_X, configurations.border_fire_3_position_y, configurations.border_fire_3_angle))


        _logger.info("Du hast Farbe {} gewählt".format(self.team_colour))
        if self.team_colour == "red":
            fahrwerk.set_position(configurations.maximum_x - configurations.start_position_x, configurations.start_position_y, configurations.start_position_angle)
        else:
            fahrwerk.set_position(configurations.start_position_x, configurations.start_position_y, configurations.start_position_angle)

        state_machine.start_time = time.time()
        start_game_over(configurations.game_time)

    @on_enter
    def enter(self):
        _logger.info("Test Flag ist jetzt: {}".format(self.route_1))

    #@transition
    #def start(self):
    #    if self.emergency_switch() and self.emergency_switch()[1][0] == 1:
    #       self.route_1 = 0


    @task
    def handle_collision(self):
        if self.quick_stopped() and "FrescoMove" not in state_machine.last_state:
            _logger.warning("Collision occoured")
        else:
            return True

    @transition
    def one(self):
        if self.route_1 == 0:
            self.current_fire_on_field = next(self.fires_on_field_iterator)
            handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
            self.route_1 = 1

            return AbsolutMove(handler.start_point.x, handler.start_point.y)

    @transition
    def two(self):
        if self.route_1 == 1:
            handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
            self.current_fire_on_field.flip(self.team_colour)
            self.route_1 = 2

            return AbsolutMove(handler.flip_point.x, handler.flip_point.y)

    @transition
    def three(self):
        if self.route_1 == 2:
            self.route_1 = 3

            return AbsolutMove(Destination(500, self.team_colour).dest_x, 1100)

    @transition
    def four(self):
        if self.route_1 == 3:
            self.route_1 = 4

            return AbsolutMove(Destination(1000, self.team_colour).dest_x, 1100)

    @transition
    def five(self):
        if self.route_1 == 4:
            self.current_fire_on_field = next(self.fires_on_field_iterator)
            handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
            self.route_1 = 5

            return AbsolutMove(handler.start_point.x, handler.start_point.y)

    @transition
    def six(self):
        if self.route_1 == 5:
            handler = StandingFireHandler(self.current_fire_on_field, self.team_colour)
            self.route_1 = 6

            return AbsolutMove(handler.flip_point.x, handler.flip_point.y)


    @task
    def done(self):
        if self.route_1 == 6:
            _logger.info("Fertig! Stoppe die Statemachine.")
            state_machine.stop()

    @task
    def problem(self):
        if self.route_1 < 0:
            _logger.info("Ein Fehler ist aufgetreten! Stoppe die Statemachine.")
            state_machine.stop()

    @on_exit
    def exit(self):
        _logger.info("Test Flag ist jetzt: {}".format(self.route_1))
        self.quick_stopped.reset()


@state
class AutorouterMove:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.autorouter = Autorouter()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.autorouter.move(self.x, self.y)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


@state
class AbsolutMove:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, False)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


@state
class KickFire:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, False)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


@state
class FrescoMove:
    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.fahrwerk = Fahrwerk()
        self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, True)

    @transition
    def done(self):
        if self.position_reached.check():
            return Intelligence()


def start_game_over(seconds):
    emergency_switch = Power().emergency_switch()
    emergency_switch.reset()

    @global_task
    def game_over():
        if (emergency_switch() and emergency_switch()[1][0] == 0) or time.time() - state_machine.start_time >= seconds:
            _logger.info("Spiel ist vorbei nach {}!".format(time.time() - state_machine.start_time))
            state_machine.stop()
            Fahrwerk().quick_stop()
            #poweroff(63)!
        else:
            return True

if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.INFO)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie zum fahren eines Pentagrams")

    master = Intelligence()
    master.start()
