#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import time
import math

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

import logging
import logger

from driver import Fahrwerk, Power, Autorouter, Sensorik, Robocom, RPS, Bildverarbeitung, Handhabung_Groß

from statemachine import state, master_state, state_machine, on_enter, transition, on_exit, task, global_task
from test_2014 import StandingFire, BorderFire, StandingFireHandler, Configurations, Destination, Possibilities, Roboter, Calc_prio, Point, Calc_prio_test, FullTree, TreeHandler, Lying_Fire

_logger = logging.getLogger(__name__)
team_colour = None
while team_colour != "red" and team_colour != "yellow":
    team_colour = input("Welche Teamfarbe (red/yellow)? \n")


minibot_team = Point(0, 0)
mainbot_enemy = Point(-1000, -1000)  #position durch RPS
minibot_enemy = Point (-1000, -1000)  #Position durch RPS

enemy_collect_fruits = None


test = None

while (test != "yes"):
    test = "yes"
    input("Notaus Gezogen? \n")

test = None
while (test != "yes"):
    test = "yes"
    input("Hall Sensor Steckt? \n")

test = None
while (test != "yes"):
    test = "yes"
    input("Akku Steckt und ist geladen? \n")

test = None
while (test != "yes"):
    test = "yes"
    input("Cangateway Läuft? \n")

while enemy_collect_fruits != "yes" and enemy_collect_fruits != "no":
    enemy_collect_fruits = input("Kann der Gegner Früchte Sammeln (yes/no)? \n")

route_1 = 0
tree_normal = 0
looking_side = 0 #looking left looking right
current_object = None
backoff_flag = 0
flip_fire = None
tree_done = None
tree_side = None
border_fire_done = None
configuration = Configurations()

standing_fires = None
standing_fires = []

lying_fires = None
lying_fires = []


#0
standing_fires.append(StandingFire(configuration.standing_fire_0_position_x, configuration.standing_fire_0_position_y, configuration.standing_fire_0_angle, 0, configuration.standing_fire_prio, configuration.standing_fire_time))
#1
standing_fires.append(StandingFire(configuration.standing_fire_1_position_x, configuration.standing_fire_1_position_y, configuration.standing_fire_1_angle, 1, configuration.standing_fire_prio, configuration.standing_fire_time))
#2
standing_fires.append(StandingFire(configuration.standing_fire_2_position_x, configuration.standing_fire_2_position_y, configuration.standing_fire_2_angle, 2, configuration.standing_fire_prio, configuration.standing_fire_time))
#3
standing_fires.append(StandingFire(configuration.standing_fire_3_position_x, configuration.standing_fire_3_position_y, configuration.standing_fire_3_angle, 3, configuration.standing_fire_prio, configuration.standing_fire_time))
#4
standing_fires.append(StandingFire(configuration.standing_fire_4_position_x, configuration.standing_fire_4_position_y, configuration.standing_fire_4_angle, 4, configuration.standing_fire_prio, configuration.standing_fire_time))
#5
standing_fires.append(StandingFire(configuration.standing_fire_5_position_x, configuration.standing_fire_5_position_y, configuration.standing_fire_5_angle, 5, configuration.standing_fire_prio, configuration.standing_fire_time))

fires_on_field_yellow  = None
fires_on_field_yellow = [standing_fires[0], standing_fires[1], standing_fires[3], standing_fires[5]]

fires_on_field_red = None
fires_on_field_red = [standing_fires[5], standing_fires[3], standing_fires[1], standing_fires[0]]

fires_on_field_iterator = None
if team_colour == "yellow":
    fires_on_field_iterator = iter(fires_on_field_yellow)
elif team_colour == "red":
    fires_on_field_iterator = iter(fires_on_field_red)

border_fires_yellow = None
border_fires_yellow = []
border_fires_yellow.append(BorderFire(configuration.border_fire_0_position_x, configuration.border_fire_0_position_y, configuration.border_fire_0_angle, 0, configuration.border_fire_prio, configuration.border_fire_time))
border_fires_yellow.append(BorderFire(configuration.border_fire_2_position_X, configuration.border_fire_2_position_y, configuration.border_fire_2_angle, 1, configuration.border_fire_prio, configuration.border_fire_time))

border_fires_red = None
border_fires_red = []
border_fires_red.append(BorderFire(configuration.border_fire_1_position_x, configuration.border_fire_1_position_y, configuration.border_fire_1_angle, 0, configuration.border_fire_prio, configuration.border_fire_time))
border_fires_red.append(BorderFire(configuration.border_fire_3_position_X, configuration.border_fire_3_position_y, configuration.border_fire_3_angle, 1, configuration.border_fire_prio, configuration.border_fire_time))

border_fires_iterator = None
if team_colour == "yellow":
    border_fires_iterator = iter(border_fires_yellow)
elif team_colour == "red":
    border_fires_iterator = iter(border_fires_red)

trees_on_field = None
trees_on_field = []

trees_on_field.append(FullTree(configuration.tree_0_position_x, configuration.tree_0_position_y, configuration.tree_0_angle, 0, configuration.tree_prio, configuration.tree_time))
trees_on_field.append(FullTree(configuration.tree_1_position_x, configuration.tree_1_position_y, configuration.tree_1_angle, 1, configuration.tree_prio, configuration.tree_time))
trees_on_field.append(FullTree(configuration.tree_2_position_x, configuration.tree_2_position_y, configuration.tree_2_angle, 2, configuration.tree_prio, configuration.tree_time))
trees_on_field.append(FullTree(configuration.tree_3_position_x, configuration.tree_3_position_y, configuration.tree_3_angle, 3, configuration.tree_prio, configuration.tree_time))

if enemy_collect_fruits == "yes":
    if team_colour == "red":
        trees_on_field[0].flag = True
        trees_on_field[1].flag = True
        trees_on_field[3].flag = True
    else:
        trees_on_field[2].flag = True
        trees_on_field[3].flag = True

possibilities = None
possibilities = []
collision_flag = 0

@master_state
class Intelligence:
    def __init__(self):
        _logger.info("Initialisiere Module")
        power = Power()
        power.initialize()
        configurations = Configurations()
        sensorik = Sensorik()
        sensorik.initialize(0, 0)
        robocom = Robocom()
        robocom.initialize()
        self.emergency_switch = power.emergency_switch()
        self.fahrwerk = Fahrwerk()
        self.fahrwerk.initialize()
        self.fahrwerk.set_speed(configurations.move_speed_max, configurations.turn_speed_max)
        self.quick_stopped = self.fahrwerk.quick_stopped()
        self.kollision = sensorik.collision()
        self.rps = RPS()
        self.rps.initialize(1)
        self.rps_position_recieved = self.rps.rps_position()
        self.rps_position_recieved.reset()
        Autorouter().initialize()

        Bildverarbeitung().initialize()
        self.handhabung = Handhabung_Groß()
        self.handhabung.initialize()
        self.debug = None
        self.index = None
        self.prio_alt = None
        self.field_side = 0
        #collision flag 0 == keine Kollision // 1 == Kollision Vorne // 2 == Kollision hinten
        sensorik.initialize(1, 0)
        start_game()
        recieve_int_number()
        recieve_float_number()
        minibot_positions()
        imgage_data_task()




        #cmd 20, teamcolor, data = 1 (gelb) data = 0 (rot)
        _logger.info("Du hast Farbe {} gewählt".format(team_colour))
        if team_colour == "red":
            self.fahrwerk.set_position(configuration.maximum_x - configuration.start_position_x, configuration.start_position_y, configuration.start_position_angle)
        else:
            self.fahrwerk.set_position(configuration.start_position_x, configuration.start_position_y, configuration.start_position_angle)

    @on_enter
    def enter(self):
        _logger.info("Test Flag ist jetzt: {}".format(route_1))

    #@transition
    #def start(self):
    #    if self.emergency_switch() and self.emergency_switch()[1][0] == 1:
    #       route_1 = 0

    @task
    def handle_collision(self):
        global route_1
        global collision_flag
        if (self.kollision() and (self.kollision()[1][0] == 1 or self.kollision()[1][0] == 0)):
            # self.kollision()[1][0] == 1 bedeutet vorne, 0 hinten

            autorouter = Autorouter()
            robot = Roboter()


            if self.kollision()[1][0] == 1:
                x = robot.x + math.cos(robot.angle*math.pi/180)*450
                y = robot.y + math.sin(robot.angle*math.pi/180)*450
                collision_flag  = 1


            if self.kollision()[1][0] == 0:
                x = robot.x - math.cos(robot.angle*math.pi/180)*450
                y = robot.y - math.sin(robot.angle*math.pi/180)*450
                collision_flag  = 2

            if x > 3000:
                x = 2999
            if x < 0:
                x = 1
            if y > 2000:
                y = 1999
            if y < 0:
                y = 1


            autorouter.makedynamicobjectcircle(1, x, y, 160)
            # intelligenz mit rps nicht nötig

            _logger.error("Collision occoured")
            self.quick_stopped.reset()

            self.kollision.reset()
            if collision_flag == 1:
                self.fahrwerk.move_relativ(-100, 0, True)
            elif collision_flag == 2:
                self.fahrwerk.move_relativ(100, 0, False)
            route_1 = 100
            return True
        else:
            return True

    @task
    def enemy_position(self):
        global mainbot_enemy
        if self.rps_position_recieved():
            mainbot_enemy.x = self.rps_position_recieved[1][0]
            mainbot_enemy.y = self.rps_position_recieved[1][1]
            Autorouter().makedynamicobjectcircle(4, mainbot_enemy.x, mainbot_enemy.y, 200)
            self.rps_position_recieved.reset()
            return True
        else:
            return True

    @task
    def enemy_kick_fire(self):
        global mainbot_enemy
        global minibot_enemy
        x_main = None
        y_main = None
        x_main = mainbot_enemy.x
        y_main = mainbot_enemy.y
        x_mini = minibot_enemy.x
        y_mini = minibot_enemy.y
        for i in range(0, 6):
            if math.fabs(standing_fires[i].x - x_main) < 50 and math.fabs(standing_fires[i].y - y_main) < 50:
                if team_colour == "red":
                    standing_fires[i].flip("yellow")
                    standing_fires[i].flag = False
                    Robocom().send_int(i, 1)
                else:
                    standing_fires[i].flip("red")
                    standing_fires[i].flag = False
                    Robocom().send_int(i, 1)
            if math.fabs(standing_fires[i].x - x_mini) < 50 and math.fabs(standing_fires[i].y - y_mini) < 50:
                if team_colour == "red":
                    standing_fires[i].flip("yellow")
                    standing_fires[i].flag = False
                    Robocom().send_int(i, 1)
                else:
                    standing_fires[i].flip("red")
                    standing_fires[i].flag = False
                    Robocom().send_int(i, 1)
        return True


    @transition
    def do_fire_a_1(self):
        global route_1
        if route_1 == 1:
            route_1 += 1
            if team_colour == "yellow":
                team_x = 400
            else:
                team_x = configuration.maximum_x - 400
            return AbsolutMove(team_x, 1000)

    @transition
    def do_fire_a_2(self):
        global route_1
        global current_object
        global flip_fire
        if route_1 == 2:
            route_1 += 1
            flip_fire = 1
            if team_colour == "yellow":
                current_object = standing_fires[0]
                team_x = 400
            else:
                current_object = standing_fires[5]
                team_x = configuration.maximum_x - 400
            return AbsolutMove(team_x, 1300)

    @transition
    def do_torch_a(self):
        global route_1
        if route_1 == 3:
            route_1 += 1
            if team_colour == "yellow":
                team_x = 1100
            else:
                team_x = configuration.maximum_x - 1100
            return AbsolutMove(team_x, 1100)

    @transition
    def do_fire_b_1(self):
        global route_1
        if route_1 == 4:
            route_1 += 1
            if team_colour == "yellow":
                team_x = 1100
            else:
                team_x = configuration.maximum_x - 1100
            return AbsolutMove(team_x, 1600)

    @transition
    def do_fire_b_2(self):
        global route_1
        global current_object
        global flip_fire
        if route_1 == 5:
            route_1 = 100
            flip_fire = 1
            if team_colour == "yellow":
                current_object = standing_fires[1]
                team_x = 700
            else:
                current_object = standing_fires[3]
                team_x = configuration.maximum_x - 700
            return AbsolutMove(team_x, 1600)

    @transition
    def seite_des_gegners_festlegen(self):
        global route_1
        self.sensorik = Sensorik()
        self.enemy_still_there = False
        self.left = 0
        self.right = 0
        self.back = 0
        if route_1 == 100:
            _logger.info("Starte Künstliche Intelligenz")
            route_1 = 101
            if collision_flag == 1:
                info = self.sensorik.sens_get_distance(1)/2
                self.right += info
                if info < 20:
                    self.enemy_still_there = True
                info = self.sensorik.sens_get_distance(2)/2
                self.left += info
                if info < 20:
                    self.enemy_still_there = True
                    self.left += info
                info = self.sensorik.sens_get_distance(3)
                self.left += info
                if info < 30:
                    self.enemy_still_there = True
                info = self.sensorik.sens_get_distance(4)
                self.right += info
                if info < 30:
                    self.enemy_still_there = True

                if self.enemy_still_there:
                    if self.right > self.left:
                        current_object.time = 10000
                        #self.sensorik.initialize(0, 0)
                        return AbsoluteTurn(self.fahrwerk.orientation + 180, 2)
                    else:
                        current_object.time = 10000
                        #self.sensorik.initialize(0, 0)
                        return AbsoluteTurn(self.fahrwerk.orientation + 180, 3)
            elif collision_flag == 2:
                info = self.sensorik.sens_get_distance(6)
                if info < 20:
                    current_object.time = 10000
                    #self.sensorik.initialize(0, 0)
                    return AbsoluteTurn(self.fahrwerk.orientation + 180, 4)
            else:
                #self.sensorik.initialize(0, 0)
                return Intelligence()


    @task
    def wahrscheinlichkeitsberechnung(self):
        global route_1
        global standing_fires
        global mainbot_enemy
        global minibot_enemy
        if route_1 == 101:

            Sensorik().initialize(0, 0)
            robot = Roboter()
            for i in range (0, 6):
                if standing_fires[i].is_standing == True:
                    #prio = Calc_prio(standing_fires[i], 3, 5 , self.collision_flag).prio
                    prio = Calc_prio_test(standing_fires[i], standing_fires[i].myprio, standing_fires[i].time, mainbot_enemy, minibot_enemy).prio

                    possibilities.append(Possibilities(standing_fires[i],prio, "stehendes Feuer{}".format(i)))
                    _logger.info("Stehendes_Feuer reingeschrieben in Möglichkeitenliste")

            if team_colour == "red":
                for i in range (0, 2):
                    if border_fires_red[i].flag == False:
                        #prio = Calc_prio(border_fires_red[i], 4, 4 , self.collision_flag).prio
                        prio = Calc_prio_test(border_fires_red[i], border_fires_red[i].myprio, border_fires_red[i].time, mainbot_enemy, minibot_enemy).prio

                        possibilities.append(Possibilities(border_fires_red[i], prio, "Rand Feuer{}".format(i)))
                        _logger.info("Rand_Feuer reingeschrieben in Möglichkeitenliste")

            if team_colour == "yellow":
                for i in range (0, 2):
                    if border_fires_yellow[i].flag == False:
                        #prio = Calc_prio(border_fires_yellow[i], 4, 4 , self.collision_flag).prio
                        prio = Calc_prio_test(border_fires_yellow[i], border_fires_red[i].myprio, border_fires_red[i].time, mainbot_enemy, minibot_enemy).prio

                        possibilities.append(Possibilities(border_fires_yellow[i], prio, "Rand Feuer{}".format(i)))
                        _logger.info("Rand_Feuer reingeschrieben in Möglichkeitenliste")

            for i in range (0, 4):
                if trees_on_field[i].flag == False:
                    #prio = Calc_prio(border_fires_yellow[i], 4, 4 , self.collision_flag).prio
                    prio = Calc_prio_test(trees_on_field[i], trees_on_field[i].myprio, trees_on_field[i].time, mainbot_enemy, minibot_enemy).prio

                    possibilities.append(Possibilities(trees_on_field[i], prio, "Baum {}".format(i)))
                    _logger.info("Baum reingeschrieben in Möglichkeitenliste")

            for i in range(0, len(lying_fires)):
                if lying_fires[i].flag == False:
                    prio = Calc_prio_test(lying_fires[i], lying_fires[i].myprio, lying_fires[i].time, mainbot_enemy, minibot_enemy).prio
                    possibilities.append(Possibilities(lying_fires[i], prio, "liegendes Feuer {}".format(i)))
                    _logger.info("Liegendes Feuer reingeschrieben in Möglichkeitenliste")


            _logger.debug("Möglichkeitenliste:")
            possibilities.sort(key=lambda possibilitie: possibilitie.prioritaet, reverse=True)
            if len(possibilities) == 0 and route_1 < 108:
                _logger.debug("Möglichkeitenliste Leer")
                route_1 = 108
            else:
                for i in possibilities:
                    _logger.debug("{} has priority {}".format(i.name, i.prioritaet))
                #while self.debug != "y":
                #    self.debug = input("Weiter? (yes) \n")

                self.debug = None
                route_1 = 102
            return True
        else:
            return True

    @task
    def waehle_idealstes_objekt(self):
        global route_1
        global current_object
        global minibot_team
        if route_1 == 102:
            self.index = 0
            _logger.info("es kommt index: {}, Listenlänge: {}".format(self.index, len(possibilities)))
            try:
                current_object = possibilities[self.index].object
            except IndexError:
                route_1 = -1
                return True

            if (current_object.x <= 1500 and current_object.y <= 1000):
                if (minibot_team.x <= 1500 and minibot_team.y <= 1000):
                    Robocom().send_int(10, 0)


            elif (current_object.x >= 1500 and current_object.y <= 1000):
                if (minibot_team.x >= 1500 and minibot_team.y <= 1000):
                    Robocom().send_int(10, 1)

            try:
                # Führe Wahl aus
                if (current_object == None):
                    route_1 = 7
                    return True
                elif (type(current_object) is StandingFire):
                    route_1 = 103
                elif (type(current_object) is BorderFire):
                    route_1 = 104
                elif (type(current_object) is FullTree):
                    route_1 = 105
                elif (type(current_object) is Lying_Fire):
                    route_1 = 106


            except Exception:
                    route_1 = -1

            return True
        else:
            return True


    @transition
    def kick_fire(self):
        global route_1
        global possibilities
        global current_object
        if route_1 == 103:
            Autorouter().makedynamicobjectcircle(2, current_object.x, current_object.y, 145)
            possibilities = []
            route_1 = 101
            return KickFire(current_object, team_colour, 1)


    @transition
    def pull_fire(self):
        global route_1
        global possibilities
        global current_object
        if route_1 == 104:
            route_1 = 101
            possibilities = []
            return PullFire(current_object, team_colour, 1)


    @transition
    def farm_tree(self):
        global route_1
        global possibilities
        global current_object
        if route_1 == 105:
            Autorouter().makedynamicobjectcircle(2, current_object.x, current_object.y, 275)
            possibilities = []
            route_1 = 107
            return FarmTree(current_object, 1)

    @transition
    def clear_fruits(self):
        global route_1
        if route_1 == 107:
            route_1 = 101
            return ClearFruits(team_colour, 1)

    @transition
    def turn_fire(self):
        global route_1
        global possibilities
        global current_object
        global lying_fires
        if route_1 == 106:
            Autorouter().makedynamicobjectcircle(2, current_object.x, current_object.y, 145)
            possibilities = []
            lying_fires = []
            route_1 = 101
            return TurnFire(current_object, team_colour, 1)

    @transition
    def go_to_img_position(self):
        global route_1
        if route_1 == 108:
            route_1 = 109
            return img_move()

    @transition
    def turn_to_img_position(self):
        global route_1
        global looking_side
        if route_1 == 109:
            route_1 = 110
            if looking_side == 1:
                angle = -140
                looking_side = 0
            else:
                angle = -40
                looking_side = 1
            return AbsoluteTurn(angle, None)

    @transition
    def make_image(self):
        global route_1
        if route_1 == 110:
            route_1 = 101
            _logger.info("Mache Foto Grins :)")
            Bildverarbeitung().find_fires()
            return Intelligence()

    @task
    def done(self):
        if route_1 == 50:
            _logger.info("Fertig! Stoppe die Statemachine.")
            state_machine.stop()

    @task
    def problem(self):
        if route_1 < 0:
            _logger.info("Ein Fehler ist aufgetreten! Stoppe die Statemachine.")
            state_machine.stop()

    # @task
    # def set_trees_as_done(self):
    #     if time.time() - state_machine.start_time >= 60:
    #         trees_on_field[0].flag = True
    #         trees_on_field[1].flag = True
    #         trees_on_field[2].flag = True
    #         trees_on_field[3].flag = True

    @on_exit
    def exit(self):
        _logger.info("Test Flag ist jetzt: {}".format(route_1))
        self.quick_stopped.reset()
        self.kollision.reset()


class Move:
    def __init__(self):
        self.fahrwerk = Fahrwerk()
        self.quick_stopped = self.fahrwerk.quick_stopped()
        self.position_reached = self.fahrwerk.position_reached()
        self.kollision = Sensorik().collision()
        self.position_reached.reset()
        self.quick_stopped.reset()
        self.kollision.reset()

    def stopped(self, collision=True):
        if collision:
            return self.position_reached() or self.quick_stopped() or self.kollision()
        else:
            return self.position_reached()

@state
class AbsolutMove(Move):
    def __init__(self, x, y):
        Move.__init__(self)

        self.x = x
        self.y = y

    @on_enter
    def enter(self):
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, False)

    @transition
    def done(self):
        global flip_fire
        global tree_done
        global current_object
        global border_fire_done
        if self.stopped(False):
            if flip_fire == 1:
                current_object.flip(team_colour)
                current_object.flag = True
                flip_fire = 0
            if tree_done == 1 or border_fire_done == 1:
                current_object.flag = False
                tree_done = 0
                border_fire_done = 0
            return Intelligence()
        if self.stopped():
            return Intelligence()

@state
class AbsolutMoveBackward(Move):
    def __init__(self, x, y):
        Move.__init__(self)

        self.x = x
        self.y = y

    @on_enter
    def enter(self):
        _logger.debug("Move backward from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, True)

    @transition
    def done(self):
        global flip_fire
        global current_object
        global tree_done
        global border_fire_done
        if self.stopped(False):
            if flip_fire == 1:
                current_object.flip(team_colour)
                current_object.flag = True
                flip_fire = 0
            if tree_done == 1 or border_fire_done == 1:
                current_object.flag = False
                tree_done = 0
                border_fire_done = 0
            return Intelligence()
        if self.stopped():
            return Intelligence()

@state
class AbsoluteTurn(Move):
    def __init__(self, orientation, clockwise):
        Move.__init__(self)
        self.orientation = orientation
        self.clockwise = clockwise

    @on_enter
    def enter(self):
        _logger.debug("Turn from {} to {}".format(self.fahrwerk.orientation, self.orientation))
        self.fahrwerk.turn(self.orientation, self.clockwise)

    @transition
    def done(self):
        if self.stopped():
            return Intelligence()


@state
class PullFire(Move):
    def __init__(self, fire, team_colour, move):
        Move.__init__(self)
        self.fire = fire
        self.move = move
        self.x = self.fire.x
        self.team_colour = team_colour
        self.y = self.fire.y
        self.autorouter = Autorouter()
        self.handhabung = Handhabung_Groß()
        self.sensorik = Sensorik()
        self.info = 0

    @on_enter
    def enter(self):
        global backoff_flag
        self.position_reached.reset()
        if self.move == 1:
            handler = StandingFireHandler(self.fire, self.team_colour)
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.flip_point_border.x, handler.flip_point_border.y))
            self.info = self.autorouter.move(handler.flip_point_border.x, handler.flip_point_border.y)

        if self.move == 2:
            handler = StandingFireHandler(self.fire, self.team_colour)
            self.fahrwerk.set_speed(100, 500)
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.flip_point_standing.x, handler.flip_point_standing.y))
            self.fahrwerk.move(handler.flip_point_standing.x, handler.flip_point_standing.y, False)

        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global backoff_flag
        global current_object
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            self.sensorik.initialize(1, 0)
            return BackOff(self.fire, self.team_colour, self.move, "BorderFire")
        elif self.stopped(False) and self.move == 1:
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            self.sensorik.initialize(0, 0)
            self.handhabung.move_to_axis_pos(900)
            _logger.info("Handhabung ist auf 900")
            return PullFire(self.fire, self.team_colour, 2)
        elif self.stopped(False) and self.move == 2:
            self.fahrwerk.set_speed(600, 500)
            current_object.flag = True
            return PullFireBack()
        elif self.stopped():
            self.fahrwerk.set_speed(600, 500)
            self.sensorik.initialize(1, 0)
            return Intelligence()

@state
class img_move(Move):
    def __init__(self):
        Move.__init__(self)
        self.autorouter = Autorouter()
        self.info = 0
        self.x = configuration.img_position_x
        self.y = configuration.img_position_y

    @on_enter
    def enter(self):
        global backoff_flag
        self.position_reached.reset()
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.info = self.autorouter.move(self.x, self.y)

        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global backoff_flag
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            return BackOff(None, None, None, "BildPosition")
        elif self.stopped(False):
            return Intelligence()
        elif self.stopped():
            return Intelligence()

@state
class KickFire(Move):
    def __init__(self, fire, team_colour, move):
        Move.__init__(self)
        self.fire = fire
        self.move = move
        self.team_colour = team_colour
        self.x = self.fire.x
        self.y = self.fire.y
        self.autorouter = Autorouter()
        self.info = 0

    @on_enter
    def enter(self):
        global backoff_flag
        if self.move ==1:
            handler = StandingFireHandler(self.fire, self.team_colour)
            self.position_reached.reset()
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.start_point.x, handler.start_point.y))
            self.info = self.autorouter.move(handler.start_point.x, handler.start_point.y)
        elif self.move == 2:
            handler = StandingFireHandler(self.fire, self.team_colour)
            self.position_reached.reset()
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.flip_point.x, handler.flip_point.y))
            #os.system('espeak "Just, Kick it"&')
            self.info = self.autorouter.move(handler.flip_point.x, handler.flip_point.y)

        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global current_object
        global backoff_flag
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            return BackOff(self.fire, self.team_colour, self.move)
        elif self.stopped(False) and self.move == 1:
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return KickFire(self.fire, self.team_colour, 2)
        elif self.stopped(False) and self.move == 2:
            current_object.flip(self.team_colour)
            current_object.flag = True
            return Intelligence()
        elif self.stopped():
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return Intelligence()

@state
class ClearFruits(Move):
    def __init__(self, team_colour, move):
        Move.__init__(self)
        self.move = move
        self.team_colour = team_colour
        self.autorouter = Autorouter()
        self.info = 0

    @on_enter
    def enter(self):
        global backoff_flag
        if self.move ==1:
            self.position_reached.reset()
            if team_colour == "red":
                _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, configuration.basket_position_x_red, configuration.basket_position_y))
                self.info = self.autorouter.move(configuration.basket_position_x_red, configuration.basket_position_y)
            else:
                _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, configuration.basket_position_x_yellow, configuration.basket_position_y))
                self.info = self.autorouter.move(configuration.basket_position_x_yellow, configuration.basket_position_y)
        elif self.move == 2:
            self.position_reached.reset()
            if team_colour == "red":
                _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, configuration.basket_position_x_red, configuration.basket_end_position_y))
                self.fahrwerk.move(configuration.basket_position_x_red, configuration.basket_end_position_y, False)
            else:
                _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, configuration.basket_position_x_yellow, configuration.basket_end_position_y))
                self.fahrwerk.move(configuration.basket_position_x_yellow, configuration.basket_end_position_y, False)

        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global current_object
        global backoff_flag
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            return BackOff(None, self.team_colour, self.move, "ClearFruits")
        elif self.stopped(False) and self.move == 1:
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return ClearFruits(self.team_colour, 2)
        elif self.stopped(False) and self.move == 2:
            return Drop_Fruits()
        elif self.stopped():
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return Intelligence()

@state
class TurnFire(Move):
    def __init__(self, fire, team_colour, move):
        Move.__init__(self)
        self.fire = fire
        self.move = move
        self.team_colour = team_colour
        self.x = self.fire.x
        self.y = self.fire.y
        self.autorouter = Autorouter()
        self.info = 0

    @on_enter
    def enter(self):
        global backoff_flag
        if self.move ==1:
            handler = StandingFireHandler(self.fire, self.team_colour)
            self.position_reached.reset()
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.start_point_turn.x, handler.start_point_turn.y))
            self.info = self.autorouter.move(handler.start_point_turn.x, handler.start_point_turn.y)
        elif self.move == 2:
            handler = StandingFireHandler(self.fire, self.team_colour)
            self.position_reached.reset()
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.turn_point.x, handler.turn_point.y))
            self.fahrwerk.move(handler.turn_point.x, handler.turn_point.y, False)
        elif self.move == 3:
            _logger.debug("Turn 10 Degree")
            self.fahrwerk.turn(self.fahrwerk.orientation + 10)

        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global current_object
        global backoff_flag
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            return BackOff(self.fire, self.team_colour, self.move, "TurnFire")
        elif self.stopped(False) and self.move == 1:
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return TurnFire(self.fire, self.team_colour, 2)
        elif self.stopped(False) and self.move == 2:
            return TurnFire(self.fire, self.team_colour, 3)
        elif self.stopped(False) and self.move == 3:
            current_object.flag = True
            return Grab_Fire()
        elif self.stopped():
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return Intelligence()

@state
class FarmTree(Move):
    def __init__(self, tree, move):
        Move.__init__(self)
        self.tree = tree
        self.move = move
        self.x = self.tree.x
        self.y = self.tree.y
        self.autorouter = Autorouter()
        self.info = 0
        self.abort = False
        self.sensorik = Sensorik()

    @on_enter
    def enter(self):
        global backoff_flag
        if self.move ==1:
            handler = TreeHandler(self.tree)
            self.position_reached.reset()
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.start_point.x, handler.start_point.y))
            self.info = self.autorouter.move(handler.start_point.x, handler.start_point.y)
        elif self.move == 2:
            self.position_reached.reset()
            self.sensorik.initialize(1, 0)
            if(self.tree.angle == 0):
                self.fahrwerk.turn(self.tree.angle+90)
            elif(self.tree.angle == 90):
                self.fahrwerk.turn(self.tree.angle-90)
            else:
                self.fahrwerk.turn(self.tree.angle)
        elif self.move == 3 and tree_side != None: # Tree Side 2 Bedeutet, die Bilderkennung hat zu wenig früchte gefunden/// Normalerweise + "and tree_side != 2"
            _logger.debug("Tree Side: {}".format(tree_side))
            handler = TreeHandler(self.tree)
            self.position_reached.reset()
            self.fahrwerk.set_speed(100, 5)
            self.move = 3
            _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, handler.farm_point.x, handler.farm_point.y))
            self.fahrwerk.move(handler.farm_point.x, handler.farm_point.y, False)
        elif tree_side != None:
            _logger.debug("FarmTree Abbruch, Tree Side: {})".format(tree_side))
            self.abort = True
        else:
            self.move = 4


        if self.info < 0:
            backoff_flag += 1

    @transition
    def done(self):
        global current_object
        global backoff_flag
        global route_1
        if backoff_flag == 2:
            backoff_flag = 0
            return GoToSavePos()
        elif self.info < 0:
            return BackOff(self.tree, team_colour, self.move, "FarmTree")
        elif self.stopped(False) and self.move == 1:
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return FarmTree(self.tree, 2)
        elif self.stopped(False) and self.move == 2:
            Bildverarbeitung().find_fruits()
            return FarmTree(self.tree, 3)
        elif self.move == 4:
            return FarmTree(self.tree, 3)
        elif self.abort:
            current_object.farmed()
            current_object.flag = True
            self.sensorik.initialize(0, 0)
            route_1 = 101
            return Intelligence()
        elif (self.stopped(False) and self.move == 3):
            current_object.farmed()
            current_object.flag = True
            self.fahrwerk.set_speed(600, 500)
            self.sensorik.initialize(0, 0)
            return GrabFruits()
        elif self.stopped():
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            self.fahrwerk.set_speed(600, 500)
            self.sensorik.initialize(0, 0)
            return Intelligence()

@state
class BackOff(Move):
    def __init__(self, object, team_colour, move = None, state_before = "KickFire"):
        Move.__init__(self)
        self.object = object
        self.team_colour = team_colour
        self.move = move
        self.state_before = state_before
       # self.test = None

    @on_enter
    def enter(self):

        #while self.test != "yes":
         #   self.test = input("Weiter?")
        #os.system('espeak -v de "Piep, Piep, Piep, Piep, Piep"&')
        self.fahrwerk.move_relativ(-30, 0, True)



    @transition
    def done(self):
        if ((self.position_reached() and (self.state_before == "KickFire")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return KickFire(self.object, self.team_colour, self.move)
        elif ((self.position_reached() and (self.state_before == "BorderFire")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return PullFire(self.object, self.team_colour, self.move)
        elif ((self.position_reached() and (self.state_before == "FarmTree")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return FarmTree(self.object, self.move)
        elif ((self.position_reached() and (self.state_before == "BildPosition")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return img_move()
        elif ((self.position_reached() and (self.state_before == "TurnFire")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return TurnFire(self.object, self.team_colour, self.move)
        elif ((self.position_reached() and (self.state_before == "ClearFruits")) or (self.kollision() and self.kollision()[1][0] == 1)):
            return ClearFruits(self.team_colour, self.move)
        elif (self.position_reached() or (self.kollision() and self.kollision()[1][0]== 0)):
            return Intelligence()


@state
class GoToSavePos(Move):
    def __init__(self):
        Move.__init__(self)

    @on_enter
    def enter(self):
        if self.fahrwerk.x <= 1500:
            self.fahrwerk.move(700, 1000)
        elif self.fahrwerk.x > 1500:
            self.fahrwerk.move(2300, 1000)

        if current_object != None:
            current_object.time = 10000

    @transition
    def done(self):
        global route_1
        if self.stopped(False):
            route_1 = 100
            return Intelligence()
        elif self.stopped():
            route_1 = 100
            return Intelligence()

@state
class GrabFruits():
    def __init__(self):
        self.handhabung = Handhabung_Groß()
        self.handhabung_signal_done = Handhabung_Groß().handhabung_done()
        self.handhabung_signal_failed = Handhabung_Groß().handhabung_failed()
        self.fahrwerk = Fahrwerk()

    @on_enter
    def enter(self):
        self.handhabung.move_to_axis_pos(0)
        time.sleep(1)
        if tree_side == 0:
            self.handhabung.grab_left_fruit()
        elif tree_side == 1:
            self.handhabung.grab_right_fruit()
        else:
            self.handhabung.grab_fruits()

    @transition
    def done(self):
        global tree_done
        global current_object
        if self.handhabung_signal_failed():
            self.fahrwerk.move_relativ(-100, 0, True)
            self.handhabung.extendt_grabber()
            time.sleep(2)
            return MoveToAxis(250)
        if self.handhabung_signal_done() and self.handhabung_signal_done()[1][0] == 224:
            if tree_done == 1:
                tree_done = 0
                current_object.farmed()
                current_object.flag = True
            self.fahrwerk.move_relativ(-200, 0, True)
            time.sleep(1)
            self.handhabung.extendt_grabber()
            time.sleep(2)
            return MoveToAxis(250)

@state
class Drop_Fruits():
    def __init__(self):
        self.handhabung = Handhabung_Groß()
        self.handhabung_signal = Handhabung_Groß().handhabung_done()

    @on_enter
    def enter(self):
        self.handhabung.drop_fruits()

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 225:
            self.handhabung.close_claw()
            time.sleep(2)
            return MoveToAxis(250)

@state
class PullFireBack():
    def __init__(self):
        self.handhabung = Handhabung_Groß()
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.fahrwerk = Fahrwerk()
        self.sensorik = Sensorik()

    @on_enter
    def enter(self):
        self.handhabung.move_to_axis_pos(400)

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 238:
            self.fahrwerk.move_relativ(-100, 0, True)
            time.sleep(1)
            self.handhabung.close_claw()
            self.sensorik.initialize(1, 0)
            return MoveToAxis(250)

@state
class MoveToAxis():
    def __init__(self, position):
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.handhabung = Handhabung_Groß()
        self.fahrwerk = Fahrwerk()
        self.position = position

    @on_enter
    def enter(self):
        self.handhabung.move_to_axis_pos(self.position)

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 238:
            self.fahrwerk.turn(self.fahrwerk.orientation - 180, False)
            return Intelligence()

@state
class Grab_Fire():
    def __init__(self):
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.handhabung = Handhabung_Groß()

    @on_enter
    def enter(self):
        self.handhabung.grab_fire_1()

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 238:
            return Grab_Fire2()

@state
class Grab_Fire2():
    def __init__(self):
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.handhabung = Handhabung_Groß()

    @on_enter
    def enter(self):
        self.handhabung.grab_fire_2()

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 238:
            return Flip_Fire()

@state
class Flip_Fire():
    def __init__(self):
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.handhabung = Handhabung_Groß()
        self.fahrwerk = Fahrwerk()

    @on_enter
    def enter(self):
        self.handhabung.flip_fire()

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 231:
            self.fahrwerk.move_relativ(100, 0, False)
            self.handhabung.move_to_axis_pos(250)
            return Intelligence()

@state
class Drop_Fire():
    def __init__(self):
        self.handhabung_signal = Handhabung_Groß().handhabung_done()
        self.handhabung = Handhabung_Groß()

    @on_enter
    def enter(self):
        self.handhabung.open_claw()

    @transition
    def done(self):
        if self.handhabung_signal() and self.handhabung_signal()[1][0] == 193:
            return Intelligence()


def start_game():
    game_start = Sensorik().game_start()
    game_start.reset()
    os.system('espeak -v de "Alles Geladen"&')
    handhabung = Handhabung_Groß()

    @global_task
    def starte_spiel():
        global route_1
        global team_colour
        if (game_start() and game_start()[1][0] == 1):
            _logger.info("Juhu ich darf starten")
            if team_colour == "red":
                Robocom().send_int(20, 0)
            else:
                Robocom().send_int(20, 1)
            state_machine.start_time = time.time()
            start_game_over(configuration.game_time)
            handhabung.move_to_axis_pos(250)
            _logger.info("Auf 250 fahren")
            time.sleep(1)
            handhabung.close_claw()
            if team_colour == "red":
                Robocom().send_int(20, 0)
            else:
                Robocom().send_int(20, 1)
            time.sleep(2)
            Sensorik().initialize(0, 0)
            route_1 = 1
        else:
            return True

def start_game_over(seconds):
    emergency_switch = Power().emergency_switch()
    emergency_switch.reset()

    @global_task
    def game_over():
        if (emergency_switch() and emergency_switch()[1][0] == 0) or time.time() - state_machine.start_time >= seconds:
            _logger.info("Spiel ist vorbei nach {}!".format(time.time() - state_machine.start_time))
            state_machine.stop()
            Fahrwerk().quick_stop()
            #poweroff(63)!
        else:
            return True


def recieve_int_number():
    recieve_number = Robocom().recieved_int()
    recieve_number.reset()
    #recieve_number()[1][0] = cmd
    #recieve_number()[1][1] = data

    @global_task
    def recieve_int():
        number = recieve_number()
        if number:
            global standing_fires
            global team_colour
            global border_fires_red
            global border_fires_yellow
            # cmd 0 - 5 sind stehende Feuer in selbiger reihenfolge
            # cmd 6, 7 sind border_fire
            # cmd 20, 1 Bedeutet Gelb, 0 bedeutet rot

            cmd = number[1][0]
            data = number[1][1]

            if (cmd <= 5 and data == 1):
                standing_fires[cmd].flip(team_colour)
                standing_fires[cmd].flag = True

            if (cmd > 5 and cmd < 8 and data == 1):
                if team_colour == "red":
                    border_fires_red[cmd-6].flag = True
                else:
                    border_fires_yellow[cmd-6].flag = True
            recieve_number.reset()
            return True
        else:
            return True

def imgage_data_task():
    bildverarbeitung = Bildverarbeitung()
    fire_data = bildverarbeitung.fire_data()
    fruit_data = bildverarbeitung.fruit_data()
    fire_data.reset()
    fruit_data.reset()


    @global_task
    def receive_image_data():
        global team_colour
        global lying_fires
        global tree_side
        fires = fire_data()
        fruits = fruit_data()
        fahrwerk = Fahrwerk()
        configuration = Configurations()
        if fires:
            _logger.debug("Feuer: {}".format(fires))

            data = fires[1]
            lenght = int(len(data)/4)

            _logger.debug("datalength: {}".format(len(data)))
            _logger.debug("datalength / 4: {}".format(lenght))

            robo_x = fahrwerk.x
            robo_y = fahrwerk.y
            robo_angle = fahrwerk.orientation
            angle = robo_angle*math.pi/180
            j = 0

            for i in range(0, lenght):
                fire_x = None
                fire_y = None
                if team_colour == "red":
                    if data[j] == 2 and data[j+1] == 2:
                        #BV hat anderes koordinaten system deswegen diese "umskalierung"
                        fire_y = -data[j+2]
                        fire_x = data[j+3]
                if team_colour == "yellow":
                    if data[j] == 1 and data[j+1] == 2:
                        fire_y = -data[j+2]
                        fire_x = data[j+3]
                if fire_x != None and fire_y != None:
                    # Umrechnung relativ zu absolut
                    fire_x = fire_x + 50
                    fire_y = fire_y + 20
                    abs_x = (robo_x + fire_x*math.cos(angle) - fire_y*math.sin(angle))
                    abs_y = (robo_y + fire_x*math.sin(angle) + fire_y*math.cos(angle))
                    lying_fires.append(Lying_Fire(abs_x, abs_y, 0, i, configuration.lying_fire_prio, configuration.lying_fire_time))
                    _logger.debug("Feuer {} mit koordinaten: {}/{}".format(i, abs_x, abs_y))
                j = j + 4

            fire_data.reset()
            return True
        if fruits:
            _logger.debug("Früchte: {}".format(fruits))
            if fruits[1][0] == 0:
                tree_side = 0
                if fruits[1][1] < 4:
                    tree_side = 2
            elif fruits[1][0] == 1:
                tree_side = 1
                if fruits[1][1] < 4:
                    tree_side = 2
            else:
                tree_side = 2

            fruit_data.reset()

            return True
        else:
            return True


def minibot_positions():
    little_pos = Robocom().roboter_position()
    little_enemy_pos = Robocom().gegner_position()
    little_pos.reset()
    little_enemy_pos.reset()
    #recieve_number()[1][0] = cmd
    #recieve_number()[1][1] = data

    @global_task
    def mini_position():
        global minibot_team
        global minibot_enemy
        little_team_position = little_pos()
        little_enemy_position = little_enemy_pos()
        if little_team_position:

            autorouter = Autorouter()
            mini_x = little_team_position[1][0]
            mini_y = little_team_position[1][1]

            minibot_team.x = mini_x
            minibot_team.y = mini_y

            autorouter.makedynamicobjectcircle(3, mini_x, mini_y, 150)
            little_pos.reset()
        if little_enemy_position:
            autorouter = Autorouter()
            enemy_x = little_enemy_position[1][0]
            enemy_y = little_enemy_position[1][1]
            minibot_enemy.x = enemy_x
            minibot_enemy.y = enemy_y
            autorouter.makedynamicobjectcircle(5, minibot_enemy.x, minibot_enemy.y, 150)
            little_enemy_pos.reset()
            return True
        else:
            return True



def recieve_float_number():
    recieve_number = Robocom().recieved_float()
    recieve_number.reset()
    #recieve_number()[1][0] = cmd
    #recieve_number()[1][1] = data

    @global_task
    def recieve_int():
        if recieve_number():
            recieve_number.reset()
            return True
        else:
            return True

if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.INFO)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie denkt Bitch")

    # master = master_state(Intelligence)()
    master = Intelligence()
    master.start()
