#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import math
import os

LIBRARY_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")

sys.path.append(LIBRARY_DIR)

import logging
import logger
from driver import Fahrwerk, Power, Autorouter, Sensorik, Robocom, RPS, Handhabung_Klein
from statemachine import state, master_state, state_machine, on_enter, transition, on_exit, task, global_task
from test_2014 import StandingFire, StandingFireHandler, Configurations, Destination, Possibilities, Roboter, Calc_prio, Point


_logger = logging.getLogger(__name__)

team_colour = None

sensorArrived = False

mainbot_team = Point(0, 0)
mainbot_enemy = Point(-1000, -1000)  #position durch RPS
minibot_enemy = Point(-1000, -1000)  #Position durch RPS

route_66_current = 0
route_66_before = 1

current_fire_on_field = None

current_object = None

waiting_time = None

backoff_flag = 0

flip_fire = None

fresco_move_done = None
fresco_move_done = False

move_away = None

wait_flag = 0

fresco_flag = 0

end_move = 0

configuration = Configurations()

use_robocom = None

while use_robocom != "yes" and use_robocom != "no":
    use_robocom = input("Robocom benutzen? (\"yes\" oder \"no\")")
    if use_robocom == "no":
        while team_colour != "red" and team_colour != "yellow":
            team_colour = input("Welche Teamfarbe?(\"yellow\" oder\"red\")")


standing_fires = None
standing_fires = []
standing_fires.append(StandingFire(configuration.standing_fire_0_position_x, configuration.standing_fire_0_position_y, configuration.standing_fire_0_angle, 0, configuration.standing_fire_prio, configuration.standing_fire_time))
standing_fires.append(StandingFire(configuration.standing_fire_1_position_x, configuration.standing_fire_1_position_y, configuration.standing_fire_1_angle, 1, configuration.standing_fire_prio, configuration.standing_fire_time))
standing_fires.append(StandingFire(configuration.standing_fire_2_position_x, configuration.standing_fire_2_position_y, configuration.standing_fire_2_angle, 2, configuration.standing_fire_prio, configuration.standing_fire_time))
standing_fires.append(StandingFire(configuration.standing_fire_3_position_x, configuration.standing_fire_3_position_y, configuration.standing_fire_3_angle, 3, configuration.standing_fire_prio, configuration.standing_fire_time))
standing_fires.append(StandingFire(configuration.standing_fire_4_position_x, configuration.standing_fire_4_position_y, configuration.standing_fire_4_angle, 4, configuration.standing_fire_prio, configuration.standing_fire_time))
standing_fires.append(StandingFire(configuration.standing_fire_5_position_x, configuration.standing_fire_5_position_y, configuration.standing_fire_5_angle, 5, configuration.standing_fire_prio, configuration.standing_fire_time))


fires_on_field_yellow  = None
fires_on_field_yellow = [standing_fires[2], standing_fires[4]]

fires_on_field_red = None
fires_on_field_red = [standing_fires[4], standing_fires[2]]

possibilities = None
possibilities = []

fires_on_field_iterator = None
if team_colour == "yellow":
    print("Iter Oben")
    fires_on_field_iterator = iter(fires_on_field_yellow)
elif team_colour == "red":
    print("Iter Oben")
    fires_on_field_iterator = iter(fires_on_field_red)

@master_state
class Intelligence:
    def __init__(self):
        global route_66_current
        global route_66_before
        global team_colour
        global fires_on_field_iterator
        global use_robocom
        _logger.info("Initialiesiere Module")
        power = Power()
        power.initialize()
        configuration = Configurations()
        Autorouter().initialize()
        sensorik = Sensorik()
        sensorik.initialize(0, 1)
        fahrwerk = Fahrwerk()
        fahrwerk.initialize()
        fahrwerk.set_speed(configuration.move_speed_little_max, configuration.turn_speed_little_max)
        self.handhabung = Handhabung_Klein()
        self.handhabung.top_row_up()
        self.handhabung.bottom_row_up()
        self.rps = RPS()
        self.rps.initialize(2)
        self.rps_position_recieved = self.rps.rps_position()
        self.rps_position_recieved.reset()
        self.quick_stopped = fahrwerk.quick_stop()
        self.emergency_switch = power.emergency_switch()
        self.collision = sensorik.collision()
        if use_robocom == "yes":
            robocom = Robocom()
            robocom.initialize()
            recieve_int_number()
        start_game()

    @on_enter
    def enter(self):
        _logger.info("enter: Route flag ist {}".format(route_66_current))

    @task
    def handle_collision(self):
        global route_66_current
        global route_66_before
        global waiting_time
        if (self.collision() and (self.collision()[1][0] == 1 or self.collision()[1][0] == 0) and "FrescoMove" not in state_machine.last_state):
            autorouter = Autorouter()
            robot = Roboter()

            if self.collision()[1][0] == 1:
                x = robot.x + math.cos(robot.angle*math.pi/180)*450
                y = robot.y + math.sin(robot.angle*math.pi/180)*450
                self.collision_flag  = 1


            if self.collision()[1][0] == 0:
                x = robot.x - math.cos(robot.angle*math.pi/180)*450
                y = robot.y - math.sin(robot.angle*math.pi/180)*450
                self.collision_flag  = 2

            if x > 3000:
                x = 2999
            if x < 0:
                x = 1
            if y > 2000:
                y = 1999
            if y < 0:
                y = 1

            autorouter.makedynamicobjectcircle(1, x, y, 200)

            _logger.error("Kollision!")
            self.collision.reset()

            route_66_before = route_66_current
            route_66_current = 99
            waiting_time = time.time()
            return True
        else:
            return True

    @task
    def move_minibot_away(self):
        global move_away
        global route_66_before
        global route_66_current
        if move_away is not None and route_66_current >= 12 and route_66_before != 12:
            route_66_before = route_66_current
            route_66_current = 50
            return True
        else:
            return True

    @task
    def enemy_position(self):
        global minibot_enemy
        if self.rps_position_recieved():
            minibot_enemy.x = self.rps_position_recieved[1][0]
            minibot_enemy.y = self.rps_position_recieved[1][1]
            Autorouter().makedynamicobjectcircle(4, minibot_enemy.x, minibot_enemy.y, 150)
            self.rps_position_recieved.reset()
            return True
        else:
            return True
    #
    # @transition
    # def go_to_spears_shoot(self):
    #     global route_66_current
    #     if route_66_current == 900:
    #         route_66_current = 2
    #         if team_colour == "red":
    #             return AbsoluteMove(configuration.maximum_x - configuration.spears_shoot_pos_x, configuration.spears_shoot_pos_y)
    #         else:
    #             return AbsoluteMove(configuration.spears_shoot_pos_x, configuration.spears_shoot_pos_y)
    #
    # @transition
    # def turn_to_spears_shoot(self):
    #     global route_66_current
    #     if route_66_current == 901:
    #         route_66_current = 3
    #         if team_colour == "red":
    #             return AbsoluteTurn(configuration.spears_shoot_angle, 4)
    #         else:
    #             return AbsoluteTurn(configuration.spears_shoot_angle, 4)
    #
    # @transition
    # def shoot_the_spears(self):
    #     global route_66_current
    #     if route_66_current == 902:
    #         route_66_current = 4
    #         return ShootSpears()

    @transition
    def fire_one_pos_1(self):
        global route_66_current
        #global route_66_before
        global current_object
        global fires_on_field_iterator
        if route_66_current == 1:
            _logger.debug("Erstes Feuer")
            current_object = next(fires_on_field_iterator)
            handler = StandingFireHandler(current_object, team_colour)
            #route_66_before = route_66_current
            route_66_current = 5
            return AbsoluteMove(handler.start_point.x, handler.start_point.y)


    @transition
    def fire_one_pos_2(self):
        global route_66_current
        #global route_66_before
        global current_object
        global flip_fire
        if route_66_current == 5:
            handler = StandingFireHandler(current_object, team_colour)
            flip_fire = 1
           # route_66_before = route_66_current
            route_66_current = 10
            return AbsoluteMove(handler.flip_point.x, handler.flip_point.y)

    @transition
    def fire_two_pos_1(self):
        global route_66_current
        #global route_66_before
        global current_object
        global fires_on_field_iterator
        if route_66_current == 6:
            Fahrwerk().set_speed(700, 100)
            current_object = next(fires_on_field_iterator)
            if current_object.is_standing == True:
                handler = StandingFireHandler(current_object, team_colour)
                #route_66_before = route_66_current
                route_66_current = 7
                return AbsoluteMove(handler.start_point.x, handler.start_point.y)
            else:
                #route_66_before = route_66_current
                route_66_current = 8
                return Intelligence()

    @transition
    def fire_two_pos_2(self):
        global route_66_current
        #global route_66_before
        global current_object
        global flip_fire
        if route_66_current == 7:
            if current_object.is_standing == True:
                handler = StandingFireHandler(current_object, team_colour)
                flip_fire = 1
                #route_66_before = route_66_current
                route_66_current = 8
                return AbsoluteMove(handler.flip_point.x, handler.flip_point.y)
            else:
                #route_66_before = route_66_current
                route_66_current = 8
                return Intelligence()

    @transition
    def do_not_disturb(self):
        global route_66_current
        #global route_66_before
        if route_66_current == 8:
            route_66_current = 120
            return AbsoluteMove(1500, 600)

    @transition
    def move_move_move1(self):
        global route_66_current
        #global route_66_before
        if route_66_current == 120:
            if(team_colour == "red"):
                route_66_current = 121
            return AbsoluteMove(700, 600)

    @transition
    def move_move_move2(self):
        global route_66_current
        #global route_66_before
        if route_66_current == 121:
            if(team_colour == "yellow"):
                route_66_current = 120
            return AbsoluteMove(2300, 600)


    @transition
    def fresco_pos_1(self):
        global route_66_current
        #global route_66_before
        global fresco_move_done
        global team_colour
        if route_66_current == 10:
            #route_66_before = route_66_current
            route_66_current = 11
            fresco_move_done = True
            if team_colour == "yellow":
                return AbsoluteMove(configuration.fresco_pos1_x, configuration.fresco_pos1_y)
            else:
                return AbsoluteMove(configuration.maximum_x-configuration.fresco_pos1_x, configuration.fresco_pos1_y)

    @transition
    def fresco_pos_2(self):
        global route_66_current
        #global route_66_before
        global team_colour
        if route_66_current == 11:
            #route_66_before = route_66_current
            Fahrwerk().set_speed(300, 100)
            Sensorik().initialize(1, 1)
            route_66_current = 12
            if team_colour == "yellow":
                return FrescoMove(configuration.fresco_pos2_x, configuration.fresco_pos2_y)
            else:
                return FrescoMove(configuration.maximum_x-configuration.fresco_pos2_x, configuration.fresco_pos2_y)

    @transition
    def fresco_pos_3(self):
        global route_66_current
        #global route_66_before
        global team_colour
        if route_66_current == 12:
            #route_66_before = route_66_current
            Fahrwerk().set_speed(700, 100)
            Sensorik().initialize(0, 1)
            route_66_current = 6
            if team_colour == "yellow":
                return AbsoluteMove(configuration.fresco_pos3_x, configuration.fresco_pos3_y)
            else:
                return AbsoluteMove(configuration.maximum_x-configuration.fresco_pos3_x, configuration.fresco_pos3_y)

    @transition
    def wait(self):
        global route_66_current
        global route_66_before
        global wait_flag
        global waiting_time
        if route_66_current == 99:
            if ((time.time() - state_machine.start_time) < configuration.net_action_time and wait_flag == 0):
                sleeping_time = time.time() - waiting_time
                if sleeping_time > 5:
                    wait_flag = 1
                    route_66_current = route_66_before
            else:
                wait_flag = 0
                route_66_current = 101
            return Intelligence()

    @transition
    def notfall_fresco_move(self):
        global route_66_current
        global route_66_before
        if route_66_current == 101:
            if not fresco_move_done:
                route_66_before = 11
            if route_66_before == 11:
                route_66_before = 12
                if team_colour == "yellow":
                    return Autorouter_move(configuration.fresco_pos1_x, configuration.fresco_pos1_y)
                else:
                    return Autorouter_move(configuration.maximum_x-configuration.fresco_pos1_x, configuration.fresco_pos1_y)
            if route_66_before == 12:
                route_66_before = 13
                Fahrwerk().set_speed(300, 100)
                Sensorik().initialize(1, 1)
                if team_colour == "yellow":
                    return FrescoMove(configuration.fresco_pos2_x, configuration.fresco_pos2_y)
                else:
                    return FrescoMove(configuration.maximum_x-configuration.fresco_pos2_x, configuration.fresco_pos2_y)
            if route_66_before == 13:
                Fahrwerk().set_speed(700, 100)
                Sensorik().initialize(0, 1)
                route_66_before = route_66_current
                if team_colour == "yellow":
                    return Autorouter_move(configuration.fresco_pos3_x, configuration.fresco_pos3_y)
                else:
                    return Autorouter_move(configuration.maximum_x-configuration.fresco_pos3_x, configuration.fresco_pos3_y)
            else:
                Fahrwerk().set_speed(700, 100)
                route_66_current = 8
                return Intelligence()

    @transition
    def position_catch_mammut(self):
        global route_66_current
        fahrwerk = Fahrwerk()
        #global route_66_before
        if route_66_current == 102:
            _logger.info("Auf Mammut zielen")
            #route_66_before = route_66_current
            route_66_current = 103
            if fahrwerk.x <= 1500:
                return Autorouter_move(configuration.mammut_catch_x, configuration.mammut_catch_y)
            else:
                return Autorouter_move(configuration.maximum_x-configuration.mammut_catch_x, configuration.mammut_catch_y)

    @transition
    def mammut_catch(self):
        global route_66_current
        #global route_66_before
        fahrwerk = Fahrwerk()
        if route_66_current == 103:
            _logger.info("Mammut fangen")
            #route_66_before = route_66_current
            route_66_current = 104
            if fahrwerk.x <= 1500:
                return AbsoluteTurn(configuration.mammut_catch_angle_red, 4)
            else:
                return AbsoluteTurn(configuration.mammut_catch_angle_yellow, 4)

    @transition
    def mv_minibot_away(self):
        global route_66_current
        global route_66_before
        global move_away
        if route_66_current == 50:
            route_66_current = route_66_before
            if move_away == 0:
                move_away = None
                return Autorouter_move(2300, 480)
            else:
                move_away = None
                return Autorouter_move(700, 480)

    @task
    def shoot_net(self):
        global route_66_current
        global route_66_before
        if route_66_current == 104:
            _logger.info("Netz abschießen")
            route_66_before = route_66_current
            route_66_current = 199
            sleeptime = time.time() - state_machine.start_time
            _logger.info("Schlafenszeit: {}".format(sleeptime))
            sleeptime = 95 - sleeptime
            _logger.info("Schlafenszeit: {}".format(sleeptime))
            if sleeptime > 5:
                time.sleep(sleeptime-5)
                self.handhabung.top_row_down()
                time.sleep(1)
                self.handhabung.fire_net()
            else:
                self.handhabung.top_row_down()
                time.sleep(1)
                self.handhabung.fire_net()
        else:
            return True

    @task
    def handle_next_state(self):
        global route_66_current
        global route_66_before
        global end_move
        if route_66_before > 0:
            if route_66_current >= 100 and route_66_current < 199:
                if time.time() - state_machine.start_time < configuration.net_action_time:
                    _logger.info("Jetzt kann ich noch was machen")
                    time.sleep(1)
                elif time.time() - state_machine.start_time < 90 and end_move == 0:
                    route_66_current = 102
                    end_move = 1
                elif end_move == 0:
                    route_66_current = 104
                    end_move = 1
            elif route_66_current == 199:
                _logger.info("Fertig! Stoppe die Statemachine nach {}!".format(time.time() - state_machine.start_time))
                state_machine.stop()
            return True
        else:
            return True

    @on_exit
    def exit(self):
         _logger.info("exit: Route flag ist {}".format(route_66_current))


class Move:
    def __init__(self):
        fahrwerk = Fahrwerk()
        self.quick_stopped = fahrwerk.quick_stopped()
        self.position_reached = fahrwerk.position_reached()
        self.collision = Sensorik().collision()
        self.position_reached.reset()
        self.quick_stopped.reset()
        self.collision.reset()

    def stopped(self, collision=True):
        if collision:
            return self.position_reached() or self.quick_stopped()
        else:
            return self.position_reached()


@state
class AbsoluteMove(Move):
    def __init__(self, x, y):
        Move.__init__(self)
        self.fahrwerk = Fahrwerk()

        self.x = x
        self.y = y

    @on_enter
    def enter(self):
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, False)

    @transition
    def done(self):
        global flip_fire
        global current_object
        global team_colour
        global use_robocom
        if self.stopped(False):
            if flip_fire == 1:
                current_object.flip(team_colour)
                current_object.flag = True
                flip_fire = 0
                if use_robocom == "yes":
                    Robocom().send_int(current_object.number, 1)
            return Intelligence()
        if self.stopped():
            return Intelligence()

@state
class ShootSpears():
    def __init__(self):
        self.handhabung = Handhabung_Klein()

    @on_enter
    def enter(self):
        global waiting_time
        self.handhabung.bottom_row_down()
        self.handhabung.top_row_down()
        self.handhabung.fire_bottom_row()
        self.handhabung.fire_top_row()
        waiting_time = time.time()

    @transition
    def done(self):
        return Put_Spears_Back()

@state
class Put_Spears_Back():
    def __init__(self):
        self.handhabung = Handhabung_Klein()
        self.waiting_to_back = 0

    @on_enter
    def enter(self):
        self.waiting_to_back = time.time() - waiting_time
        if self.waiting_to_back > 10:
            self.handhabung.bottom_row_up()
            time.sleep(1)
            self.handhabung.top_row_up()

    @transition
    def done(self):
        if self.waiting_to_back > 12:
            return Intelligence()

@state
class AbsoluteTurn(Move):
    def __init__(self, orientation, clockwise):
        Move.__init__(self)
        self.fahrwerk = Fahrwerk()
        self.orientation = orientation
        self.clockwise = clockwise

    @on_enter
    def enter(self):
        _logger.debug("Turn from {} to {}".format(self.fahrwerk.orientation, self.orientation))
        self.fahrwerk.turn(self.orientation, self.clockwise)

    @transition
    def done(self):
        if self.stopped():
            return Intelligence()


@state
class FrescoMove(Move):
    def __init__(self, x, y):
        Move.__init__(self)
        self.x = x
        self.y = y

        self.fahrwerk = Fahrwerk()
        #self.position_reached = self.fahrwerk.get_signal_condition("position_reached")

    @on_enter
    def enter(self):
        #self.position_reached.reset()

        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.fahrwerk.move(self.x, self.y, True)

    @transition
    def done(self):
        if self.stopped():
            return Intelligence()

@state
class Autorouter_move(Move):
    def __init__(self, x, y):
        Move.__init__(self)
        self.x = x
        self.y = y
        self.autorouter = Autorouter()
        self.fahrwerk = Fahrwerk()
        self.info = 0

    @on_enter
    def enter(self):
        self.position_reached.reset()
        _logger.debug("Move from ({}/{}) to ({}/{})".format(self.fahrwerk.x, self.fahrwerk.y, self.x, self.y))
        self.info = self.autorouter.move(self.x, self.y)

    @transition
    def done(self):
        global current_object
        if self.info < 0:
            return GoToSavePos()
        elif self.stopped(False):
            return Intelligence()
        elif self.stopped():
            self.autorouter.makedynamicobjectcircle(2, -1000, -1000, 140)
            return Intelligence()

@state
class GoToSavePos(Move):
    def __init__(self):
        Move.__init__(self)
        self.fahrwerk = Fahrwerk()

    @on_enter
    def enter(self):
        if self.fahrwerk.x <= 1500:
            self.fahrwerk.move(900, 700)
        elif self.fahrwerk.x > 1500:
            self.fahrwerk.move(2100, 700)

    @transition
    def done(self):
        if self.stopped(False):
            return Intelligence()
        elif self.stopped():
            return Intelligence()


def start_game():
    global use_robocom
    if use_robocom == "yes":
        game_start_robocom = Robocom().game_start_robocom()
        game_start_robocom.reset()
        os.system('espeak -v de "Alles Geladen"&')

    @global_task
    def game_begin():
        global route_66_current
        global sensorArrived
        global team_colour
        global use_robocom
        configuration = Configurations()
        if use_robocom == "yes":
            if (game_start_robocom() and game_start_robocom()[1][0] == 1):
                sensorArrived = True
                game_start_robocom.reset()
        #if ((sensorArrived or use_robocom == "no") and team_colour != None):
        if use_robocom == "no" or team_colour != None:
            route_66_current = 1
            _logger.info("Wir sind Team {}!".format(team_colour))
            if team_colour == "red":
                Fahrwerk().set_position(configuration.maximum_x - configuration.start_position_little_x, configuration.start_position_little_y, configuration.start_position_little_angle + 180)
            else:
                Fahrwerk().set_position(configuration.start_position_little_x, configuration.start_position_little_y, configuration.start_position_little_angle)

            state_machine.start_time = time.time()
            start_game_over(configuration.game_time + 5)
        else:
            return True


def start_game_over(seconds):
    emergency_switch = Power().emergency_switch()
    emergency_switch.reset()

    @global_task
    def game_over():
        if (emergency_switch() and emergency_switch()[1][0] == 0) or time.time() - state_machine.start_time >= seconds:
            _logger.info("Spiel ist vorbei nach {}!".format(time.time() - state_machine.start_time))
            state_machine.stop()
            Fahrwerk().quick_stop()
            #poweroff(63)!
        else:
            return True

def recieve_int_number():
    recieve_number = Robocom().recieved_int()
    recieve_number.reset()
    fahrwerk = Fahrwerk()
    #recieve_number()[1][0] = cmd
    #recieve_number()[1][1] = data

    @global_task
    def recieve_int():
        if recieve_number():
            global standing_fires
            global team_colour
            global move_away
            global boarder_fires_red
            global boarder_fires_yellow
            global fires_on_field_iterator
            # cmd 0 - 5 sind stehende Feuer in selbiger reihenfolge
            # cmd 6, 7 sind border_fire
            # cmd 10, 0 fahre in feldposition 0 // 1, fahre in Feldposition 1
            # cmd 20, 1 Bedeutet Gelb, 0 bedeutet rot

            cmd = recieve_number()[1][0]
            data = recieve_number()[1][1]
            _logger.info("Signal empfangen: ({}///{})".format(cmd, data))

            if (cmd <= 5 and data == 1):
                standing_fires[cmd].flip(team_colour)
                standing_fires[cmd].flag = True

            if (cmd > 5 and cmd < 8 and data == 1):
                if team_colour == "red":
                    boarder_fires_red[cmd-6].flag = True
                else:
                    boarder_fires_yellow[cmd-6].flag = True
            if (cmd == 10):
                if (data == 0):
                    if (fahrwerk.x < 1500 and fahrwerk.y < 1000):
                        move_away = 0

                elif (data == 1):
                    if (fahrwerk.x >= 1500 and fahrwerk.y <= 1000):
                        move_away = 1

            if (cmd == 20 and data == 1):
                team_colour = "yellow"
                fires_on_field_iterator = iter(fires_on_field_yellow)
            if (cmd == 20 and data == 0):
                team_colour = "red"
                fires_on_field_iterator = iter(fires_on_field_red)

            recieve_number.reset()
            return True
        else:
            return True

def minibot_positions():
    big_pos = Robocom().roboter_position()
    big_enemy_pos = Robocom().gegner_position()
    big_pos.reset()
    big_enemy_pos.reset()
    #recieve_number()[1][0] = cmd
    #recieve_number()[1][1] = data

    @global_task
    def mini_position():
        global mainbot_team
        global mainbot_enemy
        big_team_position = big_pos()
        big_enemy_position = big_enemy_pos()
        if big_team_position:

            autorouter = Autorouter()
            big_x = big_team_position[1][0]
            big_y = big_team_position[1][1]

            mainbot_team.x = big_x
            mainbot_team.y = big_y

            autorouter.makedynamicobjectcircle(3, big_x, big_y, 200)
            big_pos.reset()
        if big_enemy_position:
            autorouter = Autorouter()
            enemy_x = big_enemy_position[1][0]
            enemy_y = big_enemy_position[1][1]
            mainbot_enemy.x = enemy_x
            mainbot_enemy.y = enemy_y
            autorouter.makedynamicobjectcircle(5, enemy_x, enemy_y, 200)
            big_enemy_pos.reset()
            return True
        else:
            return True


if __name__ == "__main__":
    logger.init_logger()

    logger.set_modul_level("statemachine", logging.INFO)
    logger.set_modul_level("driver", logging.DEBUG)

    _logger.info("Strategie für Moguera V \nDer Roboter fährt 2 Feuer um \n und bringt Fresco's an")

    master = Intelligence()
    master.start()