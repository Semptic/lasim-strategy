# -*- coding:utf-8 -*-
import logging
from logging import handlers
import re

# TODO Factor Color Stuff out into new class
class ColoredFormatter(logging.Formatter):
    COLORS = {
        "black": 0,
        "red": 1,
        "green": 2,
        "yellow": 3,
        "blue": 4,
        "magenta": 5,
        "cyan": 6,
        "light-gray": 7,
        "default": 9,
        "gray": 60,
        "light-red": 61,
        "light-green": 62,
        "light-yellow": 63,
        "light-blue": 64,
        "light-magenta": 65,
        "light-cyan": 66,
        "white": 67
    }

    LEVEL = {
        # Log-Level Textfarbe               Hintergrundfarbe
        "DEBUG": (COLORS["gray"], COLORS["default"]),
        "INFO": (COLORS["black"], COLORS["default"]),
        "WARNING": (COLORS["yellow"], COLORS["default"]),
        "ERROR": (COLORS["light-red"], COLORS["default"]),
        "CRITICAL": (COLORS["light-gray"], COLORS["light-red"])
    }

    ANSI_SEQ = "\033[{}m"

    CODES = {
        "reset": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(0),
        "b": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(1),
        "/b": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(22),
        "color": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(30 + ColoredFormatter.COLORS[attribute]),
        "/color": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(39),
        "bg": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(40 + ColoredFormatter.COLORS[attribute]),
        "/bg": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(49),
        "level": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(
            "{};{}".format(30 + ColoredFormatter.LEVEL[log_level][0], 40 + ColoredFormatter.LEVEL[log_level][1])),
        "/level": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format("{};{}".format(39, 49)),
        "underline": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(4),
        "/underline": lambda attribute, log_level: ColoredFormatter.ANSI_SEQ.format(24)
    }

    PREFIX = "<"
    POSTFIX = ">"

    def parse_tag(self, tag, log_level):
        code = ""
        if self.colored:
            splitted = tag.split("=")
            if len(splitted) == 2:
                code = self.CODES[splitted[0]](splitted[1], log_level)
            elif len(splitted) == 1:
                code = self.CODES[splitted[0]](None, log_level)

        return code

    def __init__(self, msg, colored=True):
        logging.Formatter.__init__(self, msg)
        self.colored = colored

    def format(self, record):
        level_name = record.levelname

        message = logging.Formatter.format(self, record)

        found = re.search("<(?P<tag>.+?)>", message)

        unkown_tags = []

        while found:
            try:
                parsed_tag = self.parse_tag(found.group("tag"), level_name)
                message = message[:found.start()] + parsed_tag + message[found.end():]
            except KeyError:
                unkown_tags.append((found.start(), found.group()))
                message = message[:found.start()] + message[found.end():]
            found = re.search("<(?P<tag>.+?)>", message)

        for i in range(len(unkown_tags) - 1, -1, -1):
            message = message[:unkown_tags[i][0]] + unkown_tags[i][1] + message[unkown_tags[i][0]:]

        return message + self.parse_tag("reset", level_name)


def init_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    log_format = "[<level><b>%(levelname)-8s</b></level>] %(message)s"
    formatter = ColoredFormatter(log_format)
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def init_file_logger(path):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    log_size = 1 * 1000 * 1000
    handler = logging.handlers.RotatingFileHandler(path, maxBytes=log_size, backupCount=2)
    log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    formatter = ColoredFormatter(log_format, False) # Auch Colored um die Tags zu entfernen.
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def set_modul_level(module, level):
    logging.getLogger(module).setLevel(level)


if __name__ == "__main__":
    init_logger()

    logger = logging.getLogger()

    logger.debug("<color=green><underline><b>Grün</b></color></underline>")


