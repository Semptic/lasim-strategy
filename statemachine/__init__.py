# -*- coding:utf-8 -*-

import logging

module_name = __name__[:len(".__init__.py")]

_logger = logging.getLogger(module_name)
_logger.setLevel(logging.INFO)

from .statemachine import state_machine
from .condition import Condition
from .decorators import state, master_state, task, global_task, transition, on_enter, on_exit