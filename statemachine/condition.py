# -*- coding:utf-8 -*-
"""
.. module:: condition
   :synopsis: Dieses Modul stellt die Basisklasse für
   alle Bedingugen (Conditions) zur Verfügung.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging
import time

_logger = logging.getLogger(__name__)


class Condition():
    """
    Basisklasse für alle Bedingungen. Daraus
    müssen alle Bedingungen abgeleited werden.

    Beispiel:
        Timer als Bedingung die zutrifft wenn
        die angegebene Zeit abgelaufen ist.

        >>> from statemachine import Condition

        >>> class Timer(Condition):
        ...     def __init__(self, timeout):
        ...         super().__init__()
        ...         self.timeout = timeout
        ...         self.start_time = None
        ...     def _start(self):
        ...         self.start_time = time.time()
        ...     def _check(self):
        ...         return True if (time.time() - self.start_time > self.timeout) else False
        Um einen Timer der nach 0.2 Sekunden abläuft zu erzeugen:
        >>> timer = Timer(0.2)
    """

    def __init__(self, timeout=None):
        """
        Die Condition mit oder ohne timeout, nachdem sie
        ungültig wird initialisieren. Eine ungültige
        Bedingung ist immer falsch.

        :param timeout: Nach wie viel Sekunden die Bedingung ungültig wird.
        ;type  timeout: int
        """
        _logger.debug("BaseCondition __init__()")
        self.__timeout = timeout
        self.__startTime = None

    def check(self):
        """
        Überprüft ob die Bedingung zutrifft.

        :returns:   True falls die Bedingung zutrifft.
        :rtype:     bool
        """
        _logger.debug("BaseCondition _check()")
        if self.__timeout is None or time.time() - self.__startTime < self.__timeout:
            return self._check()
        else:
            _logger.debug("BaseCondition timeout!")
            self.stop()

        return False

    def _check(self):
        """
        Die Logik, die überprüft ob die Bedingung zutrifft.
        Diese Funktion muss von der Kinderklasse überschrieben
        werden.

        :raises NotImplementedError: wenn sie nicht von der Kinderklasse überschrieben wurde.
        """
        raise NotImplementedError

    def reset(self):
        """
        Setzt die Bedingung zurück.
        """
        _logger.debug("BaseCondition reset()")
        self.stop()
        self.start()

    def start(self):
        """
        Schaltet die Bedingung ein und initialisiert sie.
        """
        _logger.debug("BaseCondition start()")
        self.__startTime = time.time()

        self._start()

    def _start(self):
        """
        Die Funktion, die von er Kinderklasse überschrieben wird
        und die Bedingung initialisiert und aktiviert.
        """
        pass

    def stop(self):
        """
        Stoppt die Bedingung und finalisiert sie.
        """
        _logger.debug("BaseCondition stop()")

        self._stop()

    # noinspection PyMethodMayBeStatic
    def _stop(self):
        """
        Die Funktion, die von er Kinderklasse überschrieben wird
        und die Bedingung finalisiert und deaktiviert.
        """
        pass

    def __str__(self):
        """
        Überschreiben der str repräsentation.

        :returns:   Name der Klasse
        :rtype:     str
        """
        return self.__class__.__name__
