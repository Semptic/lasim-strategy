# -*- coding:utf-8 -*-
import inspect
from statemachine import state_machine

from statemachine.state import State


def state(state_class):
    class StateFactory:
        def __init__(self, *args, **kwargs):
            self.args = args
            self.kwargs = kwargs

            if not self.args and not self.kwargs:
                self.name = "{}()".format(state_class.__name__)
            elif self.args and not self.kwargs:
                self.name = "{}({})".format(
                    state_class.__name__,
                    ",".join(map(str,self.args)))
            elif not self.args and self.kwargs:
                self.name = "{}({})".format(
                    state_class.__name__,
                    ",".join(["{}={}".format(key, value) for key, value in self.kwargs.items()]))
            else:
                self.name = "{}({},{})".format(
                    state_class.__name__,
                    ",".join(map(str,self.args)),
                    ",".join(["{}={}".format(key, value) for key, value in self.kwargs.items()])
                )

        def create_state(self):


            state = State(self.name)

            for name, function in inspect.getmembers(state_class(*self.args, **self.kwargs), predicate=inspect.ismethod):
                if hasattr(function, "is_enter") and function.is_enter:
                    state._enter = function
                elif hasattr(function, "is_exit") and function.is_exit:
                    state._exit = function
                elif hasattr(function, "is_transition") and function.is_transition:
                    state._transitions[function.priority].append(function)
                elif hasattr(function, "is_task") and function.is_task:
                    state._tasks[function.priority].append(function)

            return state

        def __str__(self):
            return self.name

    return StateFactory


_master_state = None


def master_state(state_class):
    class StateFactory:
        def __init__(self, *args, **kwargs):
            self.args = args
            self.kwargs = kwargs

        def create_state(self):
            global _master_state
            if _master_state is None:
                _master_state = State(state_class.__name__, is_master_state=True)

                for name, function in inspect.getmembers(state_class(*self.args, **self.kwargs), predicate=inspect.ismethod):
                    if hasattr(function, "is_enter") and function.is_enter:
                        _master_state._enter = function
                    elif hasattr(function, "is_exit") and function.is_exit:
                        _master_state._exit = function
                    elif hasattr(function, "is_transition") and function.is_transition:
                        _master_state._transitions[function.priority].append(function)
                    elif hasattr(function, "is_task") and function.is_task:
                        _master_state._tasks[function.priority].append(function)

            return _master_state

        def start(self):
            state_machine.start(self)

        def stop(self):
            state_machine.stop()

        def __str__(self):
            return state_class.__name__

    return StateFactory


def task(priority):
    if inspect.isfunction(priority):
        wrapper = priority

        wrapper.is_task = True
        wrapper.priority = 5
    else:
        def wrapper(function):
            function.is_task = True
            function.priority = priority

            return function

    return wrapper


def global_task(priority):
    if inspect.isfunction(priority):
        state_machine.register_global_task(priority, 5)
    else:
        def wrapper(function):
            state_machine.register_global_task(function, priority)

            return function

        return wrapper


def transition(priority):
    if inspect.isfunction(priority):
        wrapper = priority

        wrapper.is_transition = True
        wrapper.priority = 5
    else:
        def wrapper(function):
            function.is_transition = True
            function.priority = priority

            return function

    return wrapper


def on_enter(function):
    function.is_enter = True

    return function


def on_exit(function):
    function.is_exit = True

    return function