# -*- coding:utf-8 -*-
"""
.. module:: condition
   :synopsis: Dieses Modul stellt die Basisklasse für
   alle Bedingugen (Conditions) zur Verfügung.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging

from statemachine import Condition


_logger = logging.getLogger(__name__)

signal_flags = {}


class Signal(Condition):
    """
    Die Signalklasse ist ein spezialfall einer Bedingung.
    Die Bedingung eines Signals trifft nur dann zu,
    wenn es von außen gesetzt wird.
    """

    def __init__(self, parameter_function):
        """
        Initialisieren mit einem timeout von
        10 Sekunden nachdem ein gesetztes Signal
        ungültig wird.
        """
        Condition.__init__(self, 10)
        self.flag = False
        self.parameter_function = parameter_function

    def _start(self):
        """
        Start setzt das Signal zurück.
        """
        self.flag = False

    def _check(self):
        """
        Nur wenn das Signal gesetzt wurde
        ist die Bedingung erfüllt.

        :returns:   Ob das Signal ausgelöst wurde.
        :rtype:     bool
        """
        return self.flag

    def set_flag(self):
        """
        Setzt das Signal.
        """
        self.reset()
        self.flag = True

    def get_parameter(self):
        return self.parameter_function()

    def __call__(self, *args, **kwargs):
        if self.check():
            return self.get_parameter()
        else:
            return False
