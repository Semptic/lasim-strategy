# -*- coding:utf-8 -*-
"""
.. module:: condition
   :synopsis: Dieses Modul stellt die Basisklasse für
   alle Bedingugen (Conditions) zur Verfügung.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
from collections import defaultdict
import logging
from statemachine import state_machine

from statemachine.transition import Transition


_logger = logging.getLogger(__name__)


# Todo: Kommentare überarbeiten
class State():
    """
    Basisklasse für alle Zustände des
    Zustandsautomaten. Jeder eigene Zustand
    muss von dieser Klasse abgeleitet werden.

    Beispiel:
        Zustand bei dem an eine Koordinate gefahren wird.
        >>> from statemachine import State
        >>> from driver import Fahrwerk

        >>> class Move(State):
        ...     def __init__(self, x, y, exit_state=None):
        ...         super().__init__(exit_state)
        ...             self.x = x
        ...             self.y = y
        ...     def _enter(self):
        ...         fahrwerk = Fahrwerk()
        ...         position_reached = fahrwerk.get_signal_condition("position_reached")
        ...         self.add_transition(position_reached, self.exit_state)
        ...         fahrwerk.move(self.x, self.y)

        Bei der Initialisierung wird die x- und y-Koordinate übergeben, an die
        gefahren werden soll und intern gespeichert. Es muss immer
        die Elternklasse mit einem Austrittszustand initialisiert werden.

        Die _enter Funktion wird beim eintreten in den Zustand ausgeführt. (Nach dieser Funktion,
        werden die Übergänge aktiviert.)
        Durch folgende Zeile wird eine Bedingung erzeugt die zutrifft, wenn ein Signal ausgelöst wurde:
        fahrwerk.get_signal_condition("position_reached")
        In diesem Fall trifft die Bedingung zu, wenn der Roboter die gewúnschte Position erreicht und
        das entsprechende Signal (position_reached) ausgelöst wurde. Die Liste der Signale kann
        der Dokumentation der jeweiligen Module entnommen werden.

        Durch
        self.add_transition(condition, state)
        wird ein Übergang definiert bei dem in den Zustand state gewechselt wird,
        wenn die Bedingung condition zutrifft. Es können auch mehrere Bedingungen
        als Liste ([cond1, cond2, condN]) übergeben werden. Diese müssen alle
        Zutreffen um in den angegebenen Zustand wechseln zu können. Übergänge
        können nur innerhalb des Zustandes angegeben werden. Sollte ein übergang
        von außen erzeugt werden, wird dieser beim eintritt in den Zustand
        gelöscht.

        Mit
        fahrwerk.move(self.x, self.y)
        wird das Fahrwerk dazu veranlasst, an die beim initialisieren übergebenen,
        Koordinaten zu fahren.

        Der aufruf dieses Zustandes erfolgt über eine Bedingung in einem anderen
        Zustand.

        >>> class SomeState(State):
        ...     def _enter(self):
        ...         self.add_transition(SomeCondition(), Move(400, 600, self))

        Beim zutreffen der SomeCondition-Bedingung wird in den Move-Zustand
        gewechselt, welcher an (400/600) Fahren soll und wenn diese Position
        erreicht ist (position_reached trifft zu) zu einem neuen SomeState
        wechselt (der aber die selben Initalisierungsparameter wie der
        ursprüngliche besitzt).

    """
    def __init__(self, name, is_master_state=False):
        """
        Initialisiert den Zustand. Die Elternklasse
        muss immer initialisiert werden um funktionieren
        zu können. Sollte kein Außtrittszustand übergeben
        werden beendet sich der Zustandsautomat nachBaseState
        durchführen der Eintrittsfunktion.

        :param exit_state:  Außtrittszustand
        :type  exit_state:  State
        """
        _logger.debug("Initalize State for {}".format(name))

        self.__entered = False

        self._tasks = defaultdict(list)

        self._transitions = defaultdict(list)

        self._next_state = self

        self.name = name

        self.__is_master_state = is_master_state

    def tick(self):
        """
        Tick wird vom Zustandsautomaten regelmäßig
        ausgeführt. Tick überprüft die definierten
        übergänge ob deren Bedingungen erfüllt sind.
        Sollte der Zustand zum
        ersten mal aufgerufen werden, wird
        die Eintrittsfunktion ausgeführt.
        """
        _logger.debug("{} tick()".format(self))

        if not self.__entered:
            self.enter()

        if self._tasks:
            sorted_priorities = sorted(self._tasks.keys())

            for priority in sorted_priorities:
                self._tasks[priority][:] = [task for task in self._tasks[priority] if task()]
                if not self._tasks[priority]:
                    del self._tasks[priority]

        if not self._check_transitions():
            return False
        else:
            return True

    def next(self):
        """
        Diese Funktion wird von dem Zustandsautomaten
        aufgerufen und gibt den nächsten Zustand zurück.
        Dies ist der Zustand selbst, falls
        keine Übergänge zutreffen.

        :returns:   Der nächste auszuführende Zustand
        :rtype:     State
        """
        _logger.debug("{} next()".format(self))

        return self._next_state

    def _check_transitions(self):
        """
        Überprüft ob die Bedingung für einen oder mehrere Übergänge
        erfüllt ist. Sollte kein Übergang erlaubt sein, wird
        nichts unternommen und der nächste Zustand ist wieder
        der aktuelle. Beim Zutreffen mehrerer wird der mit der
        höchsten Priorität gewählt, sollte auch diese gleich
        sein wird zufällig ein Zustand ausgewählt.
        Ein Sonderfall ist wenn explizit ein Zustand
        mit set_next_state gesetzt wurde. Sollte dies
        der Fall sein, wird dieser immer als
        nächster Zustand gewählt und der aktuelle
        Zustand verlassen.

        Sollte in einen anderen Zustand gewechselt werden
        wird immer die Austrittsfunktion aufgerufen.
        """
        _logger.debug("{} _check_transitions()".format(self))

        if self._transitions:
            sorted_priorities = sorted(self._transitions.keys())

            for priority in sorted_priorities:
                for transition in self._transitions[priority]:
                    next_state = transition()
                    if next_state is not None:
                        self.exit(next_state)
                        return True
            return False
        else:
            _logger.debug("{} no transitions!".format(self))
            self.exit(state_machine.master_state)
            return True

    def enter(self):
        """
        Die Eintrittsfunktion die bei dem Eintritt in
        den Zustand ausgeführt wird. Es werden Initialisierungen
        durchgeführt und die Eintrittsfunktion der Kinderklasse
        ausgeführt. Nach dem ausführen
        dieser werden die Übergänge aktiviert.
        """
        _logger.debug("{} enter()".format(self))

        self.__entered = True

        self._enter()

    def _enter(self):
        """
        Die von den Kinderklassen zu überschreibende
        Eintrittsfunktion. In dieser Funktion müssen
        die Initialisierungen, wie das setzen von
        Übergängen durchgeführt werden.
        """
        pass

    def exit(self, next_state):
        """
        Die Austrittsfunktion die beim verlassen des
        Zustands ausgeführt wird. Es werden finalisierungen,
        der reset der Klasse und die Austrittsfunktion
        der Kinderklasse ausgeführt. Nach dem ausführen
        dieser werden die Übergänge deaktiviert.
        """
        _logger.debug("{} exit()".format(self))

        self._next_state = next_state

        self._exit()

        if self.__is_master_state:
            self.__entered = False

        _logger.info("{} switches context to {}".format(self, self._next_state))

    def _exit(self):
        """
        Die von den Kinderklassen zu überschreibende
        Austrittsfunktion. In dieser Funktion müssen
        die Finalisierungen durchgeführt werden.
        """
        pass

    def __str__(self):
        """
        Überschreiben der str repräsentation.

        :returns:   Name der Klasse
        :rtype:     str
        """
        return self.name