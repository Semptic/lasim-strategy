# -*- coding:utf-8 -*-
"""
.. module:: statemachine
   :synopsis: Dieses Modul enthält den
   Zustandsautomaten, der für das ausführen und
   wechseln von Zuständen zuständig ist.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
from collections import defaultdict

try:
    import gobject
except ImportError:
    # noinspection PyPep8Naming
    from gi.repository import GObject as gobject

import logging


_logger = logging.getLogger(__name__)


class StateMachine:
    """
    Zustandsautomat, der das ausführen von Zuständen überwacht und
    die Übergänge durchführt.
    """
    loop = gobject.MainLoop()

    def __init__(self, timeout=10, interrupt_timeout=1):
        """
        Initialisieren des Zustandsautomaten. Es wird dabei der
        erste Zustand und die Abstände zwischen den überprüfungen
        der Übergänge und Interrupts gesetzt.

        :param start_state: Der erste Zustand in den gesprungen wird.
        :type  start_state: State

        :param timeout:     Abstände zwischen den Überprüfungen der Übergänge in ms.
        :type  timeout:     int

        :param interrupt_timeout: Abstände zwischen den Überprüfungen der Interrupts in ms.
        :type  interrupt_timeout: int
        """
        self.current_state = None
        self.start_state = None
        self.timeout = timeout
        self.interrupt_timeout = interrupt_timeout
        self.__started = False
        self.master_state = None
        self.__global_tasks = defaultdict(list)
        self.start_time = 0
        self.last_state = None

    @property
    def started(self):
        return self.__started

    def start(self, start_state):
        """
        Startet den Zustandsautomaten.
        """
        if not self.started:
            _logger.debug("Start state machine")
            self.master_state = start_state

            gobject.timeout_add(self.timeout, self.tick)
            gobject.timeout_add(self.interrupt_timeout, self.handle_global_tasks)

            try:
                self.loop.run()
            except KeyboardInterrupt:
                _logger.error("Stopped on keyboard interrupt")

    def tick(self):
        """
        Ausführen von Zuständen und setzen des Zustands, der beim
        nächsten durchlauf ausgeführt werden soll.
        """
        if not self.__started:
            self.__started = True
            self.current_state = self.master_state.create_state()

        _logger.debug("")
        _logger.debug("=" * 40)
        _logger.debug("Strategy tick")
        _logger.debug("Current State = " + str(self.current_state))

        try:
            next_state = self.current_state.tick()

            if next_state:
                if self.current_state.next() is not None:
                    self.last_state = str(self.current_state)
                    self.current_state = self.current_state.next().create_state()
                    _logger.debug("Next State = {}".format(self.current_state))
                    _logger.debug("=" * 40)
                else:
                    self.stop()
                    return False
        except KeyboardInterrupt:
            pass
        except Exception as e:
            self.stop()
            raise e

        return True

    @staticmethod
    def stop():
        """
        Stoppt den Zustandsautomaten.
        """
        StateMachine.loop.quit()

    def register_global_task(self, function, priority):
        """
        Anlegen eines Interrupts. Es wird zyklisch auf Interrupts
        überprüft und bei erfüllen der Bedingungen wird eine
        Funktion ausgeführt. Interrupts können nicht überschrieben
        werden, es muss der alte zuerst gelöscht werden.

        :param name:    Name des Interupts, wird für das Löschen benötigt.
        :type  name:    str

        :param conditions:  Bedingung(en) wann der Interrupt ausgelöst wird.
        :type  conditions:  Conditon

        :param function:    Die Funktion die aufgerufen wird.
        :type  function:    function

        :raises AlreadyRegistered: wenn der Interrupt bereits registriert ist.
        """
        self.__global_tasks[priority].append(function)

    def handle_global_tasks(self):
        """
        Überprüft alle registrierten Interrupts und führt sie gegebenenfalls aus.
        """
        _logger.debug("")
        _logger.debug("+" * 40)
        _logger.debug("Interrupt tick")
        try:
            if self.__global_tasks:
                sorted_priorities = sorted(self.__global_tasks.keys())

                for priority in sorted_priorities:
                    self.__global_tasks[priority][:] = [task for task in self.__global_tasks[priority] if task()]
                    if not self.__global_tasks[priority]:
                        del self.__global_tasks[priority]
        except KeyboardInterrupt:
            return False
        except Exception as e:
            self.stop()
            raise e

        _logger.debug("+" * 40)

        return True


state_machine = StateMachine()
