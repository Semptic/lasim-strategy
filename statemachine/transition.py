# -*- coding:utf-8 -*-
"""
.. module:: transition
   :synopsis: Dieses Modul stellt die funktionalität
   für Übergänge bereit.

.. moduleauthor:: Stefan Kuhn <skuhn@hm.edu>
"""
import logging

_logger = logging.getLogger(__name__)


class Transition:
    """
    Diese Klasse ermöglicht das Verwalten von Übergängen.
    """
    __TRANSITION_DEFAULT_PRIORITY = 5

    def __init__(self, conditions, state, priority=None):
        """
        Initialisieren des Überganges.

        :param conditions:  Eine oder mehrere Bedingungen.
        :type  conditions:   Condition oder tupel

        :param state:       Zustand in den gewechselt werden soll.
        :type  state:       State

        :param priority:    Priorität, wenn mehrere Übergänge gleichzeitig zutreffen.
        :type  priority:    int
        """
        try:
            conditions = tuple(conditions)
        except TypeError:
            conditions = (conditions,)

        self.__conditions = conditions
        self.__state = state

        # Wenn keine Priorität festgelegt, Standard priorität festlegen
        if priority is None:
            self.__priority = self.__TRANSITION_DEFAULT_PRIORITY
        else:
            self.__priority = priority

    def check(self):
        """
        Überprüfen ob alle Bedingungen zutreffen. (Nur wenn
        alle zutreffen werden die Bedingungen zurück gesetzt)
        """
        for condition in self.__conditions:
            if not condition.check():
                break
        else:
            return True

        return False

    @property
    def priority(self):
        return self.__priority

    @property
    def state(self):
        return self.__state

    def reset(self):
        """
        Übergang zurücksetzen.
        """
        self.stop()
        self.start()

    def start(self):
        """
        Aktivieren des Übergangs und aller Bedingungen.
        """
        for condition in self.__conditions:
            condition.start()

    def stop(self):
        """
        Deaktivieren des Übergangs und aller Bedingungen.
        """
        for condition in self.__conditions:
            condition.stop()
