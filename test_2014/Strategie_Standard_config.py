from driver import Fahrwerk
import math

class Configurations:
    def __init__(self):
        self.turn_speed_max = 600
        self.move_speed_max = 500
        self.turn_speed_little_max = 100
        self.move_speed_little_max = 700
        self.standing_fire_0_position_x = 400
        self.standing_fire_0_position_y = 1100
        self.standing_fire_0_angle = 0
        self.standing_fire_1_position_x = 900
        self.standing_fire_1_position_y = 1600
        self.standing_fire_1_angle = 90
        self.standing_fire_2_position_x = 900
        self.standing_fire_2_position_y = 600
        self.standing_fire_2_angle = -90
        self.standing_fire_3_position_x = 2100
        self.standing_fire_3_position_y = 1600
        self.standing_fire_3_angle = 90
        self.standing_fire_4_position_x = 2100
        self.standing_fire_4_position_y = 600
        self.standing_fire_4_angle = -90
        self.standing_fire_5_position_x = 2600
        self.standing_fire_5_position_y = 1100
        self.standing_fire_5_angle = 180

        self.border_fire_0_position_x = 0
        self.border_fire_0_position_y = 800
        self.border_fire_0_angle = -90
        self.border_fire_1_position_x = 1300
        self.border_fire_1_position_y = 2000
        self.border_fire_1_angle = 0
        self.border_fire_2_position_X = 1700
        self.border_fire_2_position_y = 2000
        self.border_fire_2_angle = 180
        self.border_fire_3_position_X = 3000
        self.border_fire_3_position_y = 800
        self.border_fire_3_angle = -90

        self.start_position_x = 165
        self.start_position_y = 155
        self.start_position_angle = 90
        self.start_position_little_x = 50
        self.start_position_little_y = 550
        self.start_position_little_angle = 0

        self.maximum_x = 3000
        self.maximum_y = 2000
        self.minimum_x = 0
        self.minimum_y = 0

        self.game_time = 90000
        self.net_action_time = 75

        self.torch_fire_0_position_x = 900
        self.torch_fire_0_position_y = 1100
        self.torch_fire_0_angle = 0
        self.torch_fire_1_position_x = 2100
        self.torch_fire_1_position_y = 1100
        self.torch_fire_1_angle = 0

        self.tree_0_position_x = 0
        self.tree_0_position_y = 1300
        self.tree_0_angle = 90
        self.tree_1_position_x = 700
        self.tree_1_position_y = 2000
        self.tree_1_angle = 0
        self.tree_2_position_x = 2300
        self.tree_2_position_y = 2000
        self.tree_2_angle = 0
        self.tree_3_position_x = 3000
        self.tree_3_position_y = 1300
        self.tree_3_angle = -90


        self.standing_fire_prio = 5
        self.standing_fire_time = 3
        self.border_fire_prio = 4
        self.border_fire_time = 4
        self.tree_prio = 4
        self.tree_time = 5
        self.lying_fire_prio = 10
        self.lying_fire_time = 4
        self.fresco_pos1_x = 1400
        self.fresco_pos1_y = 600
        self.fresco_pos2_x = 1400
        self.fresco_pos2_y = 0
        self.fresco_pos3_x = 1400
        self.fresco_pos3_y = 600
        self.mammut_catch_x = 694  #694
        self.mammut_catch_y = 861    #861
        self.mammut_catch_angle_red = -77.3 # -102.7
        self.mammut_catch_angle_yellow = -102.7 #-77.3
        self.spears_shoot_pos_x = 750
        self.spears_shoot_pos_y = 570
        self.spears_shoot_angle = -95

        self.tree_position_x_1 = [465, 700, 2300, 3000-465]
        self.tree_position_x_2 = [365, 700, 2300, 3000-465]
        self.tree_position_y_1 = [1300, 2000-465, 2000-465, 1300]
        self.tree_position_y_2 = [1300, 2000-365, 2000-365, 1300]

        self.basket_position_x_yellow = 3000 - 750
        self.basket_position_x_red = 750
        self.basket_position_y = 515
        self.basket_end_position_y = 470
        self.img_position_x = 1500
        self.img_position_y = 1600

class Destination:
    def __init__(self, x, colour):
        self.x = x
        self.team_colour = colour
        self.dest_x = None
        self.calc()

    def calc(self):
        if self.team_colour == "yellow":
            self.dest_x = self.x

        if self.team_colour == "red":
            self.dest_x = 3000 - self.x

class Possibilities:
    def __init__(self, object, prioritaet, name):
        self.object = object
        self.prioritaet = prioritaet
        self.name = name


class Roboter:
    def __init__(self):
        self.fahrwerk = Fahrwerk()
        self.x = self.fahrwerk.x
        self.y = self.fahrwerk.y
        self.angle = self.fahrwerk.orientation


        
class Calc_prio:
    def __init__(self, object, my_prio, time, flag):
        self.object = object
        self.time = time
        self.my_prio = my_prio
        self.flag = flag
        self.prio = None
        self.calc()
        
        
    def calc(self):
        self.object.calcdistance()
        distance = (self.object.distance)/1000
        robot = Roboter()
        
        if self.flag == 0:
            self.prio = self.my_prio/(self.time*distance)
        else:
            if self.flag == 1:
                x = robot.x + math.cos(robot.angle*math.pi/180)*300
                y = robot.y + math.sin(robot.angle*math.pi/180)*300
            if self.flag == 2:
                x = robot.x - math.cos(robot.angle*math.pi/180)*300
                y = robot.y - math.sin(robot.angle*math.pi/180)*300
            
            if (x > 1500 and y > 1000):
                if (self.object.x > 1500 and self.object.y > 1000):
                    self.prio = self.my_prio/(self.time*distance*1000)
                else:
                    self.prio = self.my_prio/(self.time*distance)
                    
            elif (x < 1500 and y > 1000):
                if (self.object.x < 1500 and self.object.y > 1000):
                    self.prio = self.my_prio/(self.time*distance*1000)
                else:
                    self.prio = self.my_prio/(self.time*distance)
                    
            elif (x < 1500 and y < 1000):
                if (self.object.x < 1500 and self.object.y < 1000):
                    self.prio = self.my_prio/(self.time*distance*1000)
                else:
                    self.prio = self.my_prio/(self.time*distance)
                    
            elif (x > 1500 and y < 1000):
                if (self.object.x > 1500 and self.object.y < 1000):
                    self.prio = self.my_prio/(self.time*distance*1000)
                else:
                    self.prio = self.my_prio/(self.time*distance)



class Calc_prio_test:
    def __init__(self, object, my_prio, time, mainbot_enemy, minibot_enemy):
        self.object = object
        self.time = time
        self.my_prio = my_prio
        self.minibot_enemy = minibot_enemy
        self.mainbot_enemy = mainbot_enemy
        self.prio = None
        self.calc()


    def calc(self):
        self.object.calcdistance()
        distance = (self.object.distance)/1000
        robot = Roboter()


        if (self.object.x >= 1500 and self.object.y >= 1000):
            if ((self.mainbot_enemy.x >= 1500 and self.mainbot_enemy.y >= 1000) or (self.minibot_enemy.x >= 1500 and self.minibot_enemy.y >= 1000)):
                self.prio = self.my_prio/(self.time*distance*1000)
            else:
                self.prio = self.my_prio/(self.time*distance)

        elif (self.object.x < 1500 and self.object.y > 1000):
            if ((self.mainbot_enemy.x < 1500 and self.mainbot_enemy.y > 1000) or (self.minibot_enemy.x < 1500 and self.minibot_enemy.y > 1000)):
                self.prio = self.my_prio/(self.time*distance*1000)
            else:
                self.prio = self.my_prio/(self.time*distance)

        elif (self.object.x < 1500 and self.object.y < 1000):
            if ((self.mainbot_enemy.x < 1500 and self.mainbot_enemy.y < 1000) or (self.minibot_enemy.x < 1500 and self.minibot_enemy.y < 1000)):
                self.prio = self.my_prio/(self.time*distance*1000)
            else:
                self.prio = self.my_prio/(self.time*distance)

        elif (self.object.x > 1500 and self.object.y < 1000):
            if ((self.mainbot_enemy.x > 1500 and self.mainbot_enemy.y < 1000) or (self.minibot_enemy.x > 1500 and self.minibot_enemy.y < 1000)):
                self.prio = self.my_prio/(self.time*distance*1000)
            else:
                self.prio = self.my_prio/(self.time*distance)










