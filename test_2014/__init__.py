# -*- coding:utf-8 -*-

import logging

module_name = __name__[:len(".__init__.py")]

_logger = logging.getLogger(module_name)
_logger.setLevel(logging.INFO)

from .fires import StandingFire, BorderFire, StandingFireHandler, Lying_Fire

from .tree import FullTree, TreeHandler

from .point import Point

from .Strategie_Standard_config import Configurations, Destination, Possibilities, Roboter, Calc_prio, Calc_prio_test
