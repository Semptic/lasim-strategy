__author__ = 'markus'

from .point import Point
import math

class Fire(Point):
    def __init__(self, x, y, angle, number, myprio, time):
        Point.__init__(self, x, y, myprio, time)
        self.angle = math.pi*angle/180
        self.colour = None
        self.is_standing = True
        self.number = number

class Lying_Fire(Fire):
    pass



class StandingFire(Fire):
    def flip(self, to_colour):
        self.colour = to_colour
        self.is_standing = False


class BorderFire(Fire):
    def pull(self, to_colour):
        self.colour = to_colour


class StandingFireHandler():
    def __init__(self, fire, team):
        self.fire = fire
        self.team = team
        self.distance = 340
        self.distance_kick = 50
        self.distance_border = 400
        self.distance_border_stand = 230
        self.start_point = None
        self.flip_point = None
        self.flip_point_border = None
        self.start_point_turn = None
        self.turn_point = None
        self.flip_point_standing = None
        self.cal_start_point()
        self.cal_flip_point()
        self.cal_flip_point_border()
        self.cal_flip_point_standing()
        self.calc_start_point_turn()
        self.calc_turn_point()

    def cal_start_point(self):
        if self.team == "yellow":
            x = self.fire.x + self.distance * math.cos(self.fire.angle - math.pi/2)
            y = self.fire.y + self.distance * math.sin(self.fire.angle - math.pi/2)
        else:
            x = self.fire.x + self.distance * math.cos(self.fire.angle + math.pi/2)
            y = self.fire.y + self.distance * math.sin(self.fire.angle + math.pi/2)
        self.start_point = Point(x, y)

    def cal_flip_point(self):
        #self.flip_point = Point(self.fire.x, self.fire.y)
        if self.team == "yellow":
            x = self.fire.x + self.distance_kick * math.cos(self.fire.angle + math.pi/2)
            y = self.fire.y + self.distance_kick * math.sin(self.fire.angle + math.pi/2)
        else:
            x = self.fire.x + self.distance_kick * math.cos(self.fire.angle - math.pi/2)
            y = self.fire.y + self.distance_kick * math.sin(self.fire.angle - math.pi/2)
        self.flip_point = Point(x, y)

    def cal_flip_point_border(self):
        #self.flip_point = Point(self.fire.x, self.fire.y)
        if self.team == "yellow":
            x = self.fire.x + self.distance_border * math.cos(self.fire.angle + math.pi/2)
            y = self.fire.y + self.distance_border * math.sin(self.fire.angle + math.pi/2)
        else:
            x = self.fire.x + self.distance_border * math.cos(self.fire.angle - math.pi/2)
            y = self.fire.y + self.distance_border * math.sin(self.fire.angle - math.pi/2)
        self.flip_point_border = Point(x, y)

    def cal_flip_point_standing(self):
        #self.flip_point = Point(self.fire.x, self.fire.y)
        if self.team == "yellow":
            x = self.fire.x + self.distance_border_stand * math.cos(self.fire.angle + math.pi/2)
            y = self.fire.y + self.distance_border_stand * math.sin(self.fire.angle + math.pi/2)
        else:
            x = self.fire.x + self.distance_border_stand * math.cos(self.fire.angle - math.pi/2)
            y = self.fire.y + self.distance_border_stand * math.sin(self.fire.angle - math.pi/2)
        self.flip_point_standing = Point(x, y)

    def calc_start_point_turn(self):
        angle = math.atan2(self.fire.y, self.fire.x)
        length = math.sqrt(self.fire.x * self.fire.x + self.fire.y * self.fire.y)
        length = length - self.distance_border
        x = length * math.cos(angle)
        y = length * math.sin(angle)
        self.start_point_turn = Point(x, y)

    def calc_turn_point(self):
        angle = math.atan2(self.fire.y, self.fire.x)
        length = math.sqrt(self.fire.x * self.fire.x + self.fire.y * self.fire.y)
        #length = length + self.distance_kick nicht mehr benötigt, da schon in der umrechnung beachtet
        x = length * math.cos(angle)
        y = length * math.sin(angle)
        self.turn_point = Point(x, y)


