__author__ = 'markus'

import math

from .Strategie_Standard_config import Roboter

class Point():
    def __init__(self, x, y, myprio = None, time = None):
        self.x = x
        self.y = y
        self.distance = None
        self.flag = False
        self.myprio = myprio
        self.time = time


    def calcdistance(self):
        robot = Roboter()
        self.distance = math.hypot(math.fabs(robot.x - self.x), math.fabs(robot.y -self.y))


