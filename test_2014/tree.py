from .point import Point
import math


class Tree(Point):
    def __init__(self, x, y, angle, number, myprio, time):
        Point.__init__(self, x, y, myprio, time)
        self.angle = math.pi*angle/180
        self.is_full = True
        self.number = number


class FullTree(Tree):
    def farmed(self):
        self.is_full = False


class TreeHandler():
    def __init__(self, tree):
        self.tree = tree
        self.distance = 500
        self.distance_farm = 265
        self.start_point = None
        self.farm_point = None
        self.cal_start_point()
        self.cal_farm_point()

    def cal_start_point(self):
        x = self.tree.x + self.distance * math.cos(self.tree.angle - math.pi/2)
        y = self.tree.y + self.distance * math.sin(self.tree.angle - math.pi/2)
        self.start_point = Point(x, y)

    def cal_farm_point(self):
        x = self.tree.x + self.distance_farm * math.cos(self.tree.angle - math.pi/2)
        y = self.tree.y + self.distance_farm * math.sin(self.tree.angle - math.pi/2)
        self.farm_point = Point(x, y)